/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/public/public.js":
/*!***************************************!*\
  !*** ./resources/js/public/public.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var baseUrl = 'http://entra.test/'; // Get the modal

var modal = document.getElementById("modalComoComprar"); // Get the button that opens the modal

var btn = document.getElementById("linkModal"); // Get the <span> element that closes the modal

var span = document.getElementsByClassName("close")[0]; // When the user clicks the button, open the modal 

btn.onclick = function () {
  modal.style.display = "block";
}; // When the user clicks on <span> (x), close the modal


span.onclick = function () {
  modal.style.display = "none";
}; // When the user clicks anywhere outside of the modal, close it


window.onclick = function (event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}; //-----------------------------------
// This example uses the autocomplete feature of the Google Places API.
// It allows the user to find all hotels in a given place, within a given
// country. It then displays markers for all the hotels returned,
// with on-click details for each hotel.
// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">


var map;
var places;
var infoWindow;
var markers = [];
var autocomplete;
var countryRestrict = {
  country: "us"
};
var MARKER_PATH = "https://developers.google.com/maps/documentation/javascript/images/marker_green";
var hostnameRegexp = new RegExp("^https?://.+?/");
var countries = {
  au: {
    center: {
      lat: -25.3,
      lng: 133.8
    },
    zoom: 4
  },
  br: {
    center: {
      lat: -14.2,
      lng: -51.9
    },
    zoom: 3
  },
  ca: {
    center: {
      lat: 62,
      lng: -110.0
    },
    zoom: 3
  },
  fr: {
    center: {
      lat: 46.2,
      lng: 2.2
    },
    zoom: 5
  },
  de: {
    center: {
      lat: 51.2,
      lng: 10.4
    },
    zoom: 5
  },
  mx: {
    center: {
      lat: 23.6,
      lng: -102.5
    },
    zoom: 4
  },
  nz: {
    center: {
      lat: -40.9,
      lng: 174.9
    },
    zoom: 5
  },
  it: {
    center: {
      lat: 41.9,
      lng: 12.6
    },
    zoom: 5
  },
  za: {
    center: {
      lat: -30.6,
      lng: 22.9
    },
    zoom: 5
  },
  es: {
    center: {
      lat: 40.5,
      lng: -3.7
    },
    zoom: 5
  },
  pt: {
    center: {
      lat: 39.4,
      lng: -8.2
    },
    zoom: 6
  },
  us: {
    center: {
      lat: 37.1,
      lng: -95.7
    },
    zoom: 3
  },
  uk: {
    center: {
      lat: 54.8,
      lng: -4.6
    },
    zoom: 5
  }
};

function initMap() {
  map = new google.maps.Map(document.getElementById("map"), {
    zoom: countries["us"].zoom,
    center: countries["us"].center,
    mapTypeControl: false,
    panControl: false,
    zoomControl: false,
    streetViewControl: false
  });
  infoWindow = new google.maps.InfoWindow({
    content: document.getElementById("info-content")
  }); // Create the autocomplete object and associate it with the UI input control.
  // Restrict the search to the default country, and to place type "cities".

  autocomplete = new google.maps.places.Autocomplete(document.getElementById("autocomplete"), {
    types: ["(cities)"],
    componentRestrictions: countryRestrict
  });
  places = new google.maps.places.PlacesService(map);
  autocomplete.addListener("place_changed", onPlaceChanged); // Add a DOM event listener to react when the user selects a country.

  document.getElementById("country").addEventListener("change", setAutocompleteCountry);
} // When the user selects a city, get the place details for the city and
// zoom the map in on the city.


function onPlaceChanged() {
  var place = autocomplete.getPlace();

  if (place.geometry) {
    map.panTo(place.geometry.location);
    map.setZoom(15);
    search();
  } else {
    document.getElementById("autocomplete").placeholder = "Enter a city";
  }
} // Search for hotels in the selected city, within the viewport of the map.


function search() {
  var search = {
    bounds: map.getBounds(),
    types: ["lodging"]
  };
  places.nearbySearch(search, function (results, status, pagination) {
    if (status === google.maps.places.PlacesServiceStatus.OK) {
      clearResults();
      clearMarkers(); // Create a marker for each hotel found, and
      // assign a letter of the alphabetic to each marker icon.

      for (var i = 0; i < results.length; i++) {
        var markerLetter = String.fromCharCode("A".charCodeAt(0) + i % 26);
        var markerIcon = MARKER_PATH + markerLetter + ".png"; // Use marker animation to drop the icons incrementally on the map.

        markers[i] = new google.maps.Marker({
          position: results[i].geometry.location,
          animation: google.maps.Animation.DROP,
          icon: markerIcon
        }); // If the user clicks a hotel marker, show the details of that hotel
        // in an info window.

        markers[i].placeResult = results[i];
        google.maps.event.addListener(markers[i], "click", showInfoWindow);
        setTimeout(dropMarker(i), i * 100);
        addResult(results[i], i);
      }
    }
  });
}

function clearMarkers() {
  for (var i = 0; i < markers.length; i++) {
    if (markers[i]) {
      markers[i].setMap(null);
    }
  }

  markers = [];
} // Set the country restriction based on user input.
// Also center and zoom the map on the given country.


function setAutocompleteCountry() {
  var country = document.getElementById("country").value;

  if (country == "all") {
    autocomplete.setComponentRestrictions({
      country: []
    });
    map.setCenter({
      lat: 15,
      lng: 0
    });
    map.setZoom(2);
  } else {
    autocomplete.setComponentRestrictions({
      country: country
    });
    map.setCenter(countries[country].center);
    map.setZoom(countries[country].zoom);
  }

  clearResults();
  clearMarkers();
}

function dropMarker(i) {
  return function () {
    markers[i].setMap(map);
  };
}

function addResult(result, i) {
  var results = document.getElementById("results");
  var markerLetter = String.fromCharCode("A".charCodeAt(0) + i % 26);
  var markerIcon = MARKER_PATH + markerLetter + ".png";
  var tr = document.createElement("tr");
  tr.style.backgroundColor = i % 2 === 0 ? "#F0F0F0" : "#FFFFFF";

  tr.onclick = function () {
    google.maps.event.trigger(markers[i], "click");
  };

  var iconTd = document.createElement("td");
  var nameTd = document.createElement("td");
  var icon = document.createElement("img");
  icon.src = markerIcon;
  icon.setAttribute("class", "placeIcon");
  icon.setAttribute("className", "placeIcon");
  var name = document.createTextNode(result.name);
  iconTd.appendChild(icon);
  nameTd.appendChild(name);
  tr.appendChild(iconTd);
  tr.appendChild(nameTd);
  results.appendChild(tr);
}

function clearResults() {
  var results = document.getElementById("results");

  while (results.childNodes[0]) {
    results.removeChild(results.childNodes[0]);
  }
} // Get the place details for a hotel. Show the information in an info window,
// anchored on the marker for the hotel that the user selected.


function showInfoWindow() {
  var marker = this;
  places.getDetails({
    placeId: marker.placeResult.place_id
  }, function (place, status) {
    if (status !== google.maps.places.PlacesServiceStatus.OK) {
      return;
    }

    infoWindow.open(map, marker);
    buildIWContent(place);
  });
} // Load the place information into the HTML elements used by the info window.


function buildIWContent(place) {
  document.getElementById("iw-icon").innerHTML = '<img class="hotelIcon" ' + 'src="' + place.icon + '"/>';
  document.getElementById("iw-url").innerHTML = '<b><a href="' + place.url + '">' + place.name + "</a></b>";
  document.getElementById("iw-address").textContent = place.vicinity;

  if (place.formatted_phone_number) {
    document.getElementById("iw-phone-row").style.display = "";
    document.getElementById("iw-phone").textContent = place.formatted_phone_number;
  } else {
    document.getElementById("iw-phone-row").style.display = "none";
  } // Assign a five-star rating to the hotel, using a black star ('&#10029;')
  // to indicate the rating the hotel has earned, and a white star ('&#10025;')
  // for the rating points not achieved.


  if (place.rating) {
    var ratingHtml = "";

    for (var i = 0; i < 5; i++) {
      if (place.rating < i + 0.5) {
        ratingHtml += "&#10025;";
      } else {
        ratingHtml += "&#10029;";
      }

      document.getElementById("iw-rating-row").style.display = "";
      document.getElementById("iw-rating").innerHTML = ratingHtml;
    }
  } else {
    document.getElementById("iw-rating-row").style.display = "none";
  } // The regexp isolates the first part of the URL (domain plus subdomain)
  // to give a short URL for displaying in the info window.


  if (place.website) {
    var fullUrl = place.website;
    var website = String(hostnameRegexp.exec(place.website));

    if (!website) {
      website = "http://" + place.website + "/";
      fullUrl = website;
    }

    document.getElementById("iw-website-row").style.display = "";
    document.getElementById("iw-website").textContent = website;
  } else {
    document.getElementById("iw-website-row").style.display = "none";
  }
}

/***/ }),

/***/ 3:
/*!*********************************************!*\
  !*** multi ./resources/js/public/public.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/html/entraypedi.new/resources/js/public/public.js */"./resources/js/public/public.js");


/***/ })

/******/ });
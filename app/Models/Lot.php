<?php

namespace App\Models;

use App\Models\Measure;
use App\Models\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lot extends Model
{
    use SoftDeletes;

	protected $table = 'lots';
	protected $primaryKey = 'id';
	protected $fillable = [

        'product_id',
        'measure_id',
        'price_sale',
        'promotion',
        'image',
    ];
    

	public $timestamps = true;
    protected $dates = ['deleted_at'];
    
    //Relationships
    public function product(){
        
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function measure(){

        return $this->belongsTo(Measure::class, 'measure_id', 'id');
    }
}


<?php

namespace App\Models;

use App\Models\Lot;
use App\Models\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Measure extends Model
{
    use SoftDeletes;

	protected $table = 'measures';
	protected $primaryKey = 'id';
	protected $fillable = [

        'category_id',
        'description',
    ];
    

	public $timestamps = true;
    protected $dates = ['deleted_at'];
    
    //Relationships
    public function category(){
        
        return $this->belongsTo(Category::class);
    }

    public function lots(){

        return $this->hasMany(Lot::class);
    }

}



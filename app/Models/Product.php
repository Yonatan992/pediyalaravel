<?php

namespace App\Models;

use App\Models\Lot;
use App\Models\Business;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

	protected $table = 'products';
	protected $primaryKey = 'id';
	protected $fillable = [

        'business_id',
        'subcategory_id',
        'name',
        'description',
        'stock',
        'price_list',
        'image',

	];

	public $timestamps = true;
    protected $dates = ['deleted_at'];
    
    //Relationships
    public function business(){

        return $this->belongsTo(Business::class, 'business_id', 'id');
    }

    public function subcategory(){

        return $this->belongsTo(Subcategory::class, 'subcategory_id', 'id');
    }

    public function lots(){

        $this->hasMany(Lot::class);
    }

}



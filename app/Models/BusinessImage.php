<?php

namespace App\Models;

use App\Models\Business;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BusinessImage extends Model
{
    use SoftDeletes;

	protected $table = 'business_images';
	protected $primaryKey = 'id';
	protected $fillable = [

        'business_id',
        'image',
    ];
    

	public $timestamps = true;
    protected $dates = ['deleted_at'];
    
    //Relationships
    public function business(){

        return $this->belongsTo(Business::class, 'state_id', 'id');
    }
    
}



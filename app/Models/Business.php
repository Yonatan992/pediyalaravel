<?php

namespace App\Models;

use App\User;
use App\Models\Item;
use App\Models\State;
use App\Models\Product;
use App\Models\Location;
use App\Models\TypeBusiness;
use App\Models\BusinessImage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Business extends Model
{
    use SoftDeletes;

	protected $table = 'businesses';
	protected $primaryKey = 'id';
	protected $fillable = [

        'item_id',
        'location_id',
        'state_id',
        'owner',
        'name',
        'phone',
        'email',
        'email_verified_at',
        'address',
        'facebook',
        'instagram',
        'image',
        'latitud',
        'longitud'
    ];
    

    protected $casts = [
		'email_verified_at' => 'datetime',
    ];

	public $timestamps = true;
    protected $dates = ['deleted_at'];
    
    //Relationships
    public function users(){
        
        return $this->hasMany(User::class);
    }

    public function item(){

        return $this->belongsTo(Item::class, 'item_id', 'id');
    }

    public function location(){

        return $this->belongsTo(Location::class, 'location_id', 'id');
    }

    public function state(){

        return $this->belongsTo(State::class, 'state_id', 'id');
    }

    public function typeBusiness(){

        return $this->belongsTo(TypeBusiness::class, 'type_business_id', 'id');
    }

    public function products(){
        
        return $this->hasMany(Product::class);
    }

    public function business_images(){
        
        return $this->hasMany(BusinessImage::class);
    }
}

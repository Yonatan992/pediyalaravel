<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeUser extends Model
{
    use SoftDeletes;

	protected $table = 'type_users';
	protected $primaryKey = 'id';
	protected $fillable = [

		'description',
	];

	public $timestamps = true;
	protected $dates = ['deleted_at'];

    //Relationships
    public function users(){
        return $this->hasMany(User::class);
    }
}

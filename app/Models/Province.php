<?php

namespace App\Models;

use App\Models\Business;
use App\Models\Location;
use App\Models\Province;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Province extends Model
{
    use SoftDeletes;

	protected $table = 'provinces';
	protected $primaryKey = 'id';
	protected $fillable = [

        'description',

	];

	public $timestamps = true;
    protected $dates = ['deleted_at'];
    
    //Relationships
    public function locations(){

        return $this->hasMany(Location::class);
    }
}



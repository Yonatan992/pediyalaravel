<?php

namespace App;

use App\Models\State;
use App\Models\Business;
use App\Models\TypeUser;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $table = 'users';
	protected $primaryKey = 'id';
	protected $fillable = [
        'name',
        'lastname',
        'phone',
        'nickname',
        'email',
		'password',
		'email_verified_at',
		'verification_token',
        'usertype_id',
        'business_id',
        'state_id',
        'image',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [

		'password',
		'remember_token',
		'token_verificacion',
	];

	public $timestamps = true;
	protected $dates = ['deleted_at'];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'email_verified_at' => 'datetime',
    ];
    

    //Relationships-----------------
    public function user_type(){

		return $this->belongsTo(TypeUser::class, 'typeuser_id', 'id');
    }
    
    public function business(){

		return $this->belongsTo(Business::class, 'business_id', 'id');
    }
    
    public function state(){

		return $this->belongsTo(State::class, 'state_id', 'id');
	}
}

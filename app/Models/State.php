<?php

namespace App\Models;

use App\User;
use App\Models\Item;
use App\Models\State;
use App\Models\Client;
use App\Models\Business;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class State extends Model
{
    use SoftDeletes;

	protected $table = 'states';
	protected $primaryKey = 'id';
	protected $fillable = [

        'description',
	];

	public $timestamps = true;
    protected $dates = ['deleted_at'];
    
    //Relationships
    public function businesses(){
        
        return $this->hasMany(Business::class);
    }

    public function clients(){

        return $this->hasMany(Client::class);
    }

}


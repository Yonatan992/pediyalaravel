<?php

namespace App\Models;

use App\User;
use App\Models\Item;
use App\Models\State;
use App\Models\Product;
use App\Models\Location;
use App\Models\HeadOrder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;

	protected $table = 'businesses';
	protected $primaryKey = 'id';
	protected $fillable = [

        'state_id',
        'location_id',
        'name',
        'lastname',
        'phone',
        'address',
        'nickname',
        'email',
        'email_verified_at',
        'password',
        'image',
    ];
    
    protected $hidden = [

		'password',
    ];
    
    protected $casts = [
		'email_verified_at' => 'datetime',
    ];

	public $timestamps = true;
    protected $dates = ['deleted_at'];
    
    //Relationships
    public function state(){

        return $this->belongsTo(State::class, 'state_id', 'id');
    }

    public function location(){

        return $this->belongsTo(Location::class, 'location_id', 'id');
    }

    public function head_orders(){

        return $this->hasMany(HeadOrder::class);
    }
}


<?php

namespace App\Models;

use App\Models\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HeadOrder extends Model
{
    use SoftDeletes;

	protected $table = 'head_orders';
	protected $primaryKey = 'id';
	protected $fillable = [

        'client_id',
        'total',
        'discount',
        'date_success',
        'date_delivered',
        'state',
    ];
    

	public $timestamps = true;
    protected $dates = ['deleted_at'];
    
    //Relationships
    public function client(){
        
        return $this->belongsTo(Client::class, 'client_id', 'id');
    }

    public function details(){

        return $this->hasMany(DetailOrder::class);
    }

}



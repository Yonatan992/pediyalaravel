<?php

namespace App\Models;

use App\Models\Client;
use App\Models\Business;
use App\Models\Province;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends Model
{
    use SoftDeletes;

	protected $table = 'locations';
	protected $primaryKey = 'id';
	protected $fillable = [

        'province_id',
        'description',

	];

	public $timestamps = true;
    protected $dates = ['deleted_at'];
    
    //Relationships
    public function province(){

        return $this->belongsTo(Province::class, 'province_id', 'id');
    }

    public function businesses(){

        return $this->hasMany(Business::class);
    }

    public function clients(){

        return $this->hasMany(Client::class);
    }
}


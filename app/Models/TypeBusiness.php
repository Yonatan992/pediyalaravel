<?php

namespace App\Models;
use App\Models\Business;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeBusiness extends Model
{
    use SoftDeletes;

	protected $table = 'type_businesses';
	protected $primaryKey = 'id';
	protected $fillable = [

        'description',
        
	];

	public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function businesses(){
        
        return $this->hasMany(Business::class);
    }

}

<?php

namespace App\Models;

use App\Models\Business;
use App\Models\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use SoftDeletes;

	protected $table = 'items';
	protected $primaryKey = 'id';
	protected $fillable = ['description'];

	public $timestamps = true;
    protected $dates = ['deleted_at'];
    
    //Relationships
    public function businesses(){
        
        return $this->hasMany(Business::class);
    }

    public function categories(){

        return $this->hasMany(Category::class);
    }

}

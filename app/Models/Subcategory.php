<?php

namespace App\Models;

use App\Models\Product;
use App\Models\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubCategory extends Model
{
    use SoftDeletes;

	protected $table = 'subcategories';
	protected $primaryKey = 'id';
	protected $fillable = [

        'category_id',
        'description',

	];

	public $timestamps = true;
    protected $dates = ['deleted_at'];
    
    //Relationships
    public function category(){

        return $this->belongsTo(Category::class, 'item_id', 'id');
    }

    public function products(){

        return $this->hasMany(Product::class);
    }

}



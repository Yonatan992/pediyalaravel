<?php

namespace App\Models;

use App\Models\Item;
use App\Models\Subcategory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

	protected $table = 'categories';
	protected $primaryKey = 'id';
	protected $fillable = [

        'item_id',
        'description',
        'image',

	];

	public $timestamps = true;
    protected $dates = ['deleted_at'];
    
    //Relationships
    public function item(){

        return $this->belongsTo(Item::class, 'item_id', 'id');
    }

    public function subcategories(){

        return $this->hasMany(Subcategory::class);
    }

    public function measures(){

        return $this->hasMany(Measure::class);
    }

}


<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CoverPage extends Model
{
    use SoftDeletes;

	protected $table = 'cover_pages';
	protected $primaryKey = 'id';
	protected $fillable = [

        'description',

    ];
    

	public $timestamps = true;
    protected $dates = ['deleted_at'];
    
    //Relationships

}





<?php

namespace App\Models;

use App\Models\HeadOrder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetailOrder extends Model
{
    use SoftDeletes;

	protected $table = 'detail_orders';
	protected $primaryKey = 'id';
	protected $fillable = [

        'head_id',
        'lot_id',
        'count',
        'price_list',
        'price_sale',
        'percent_discount',
        'subtotal',
    ];
    

	public $timestamps = true;
    protected $dates = ['deleted_at'];
    
    //Relationships
    public function head_order(){
        
        return $this->belongsTo(HeadOrder::class, 'head_id', 'id');
    }

}




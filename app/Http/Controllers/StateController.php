<?php

namespace App\Http\Controllers;

use App\Models\State;
use Illuminate\Http\Request;
use App\Http\Requests\StoreState;
use App\Http\Requests\UpdateState;


class StateController extends Controller
{
     public function index()
    {
        $estados = State::orderBy('id','DESC')->get();
       	
    }

    public function show(State $state)
    {
        
    }

    public function store(StoreState $request)
    {
         $estado = State::create($request->validated());
    }

    public function update(UpdateState $request, State $state)
    {
        $state->fill($request->validated())->save();

    }

    public function destroy(State $state)
    {
        $state->delete();
       
    }
}

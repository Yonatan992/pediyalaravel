<?php

namespace App\Http\Controllers;

use App\Models\Item;
use Illuminate\Http\Request;
use App\Http\Requests\StoreItem;
use App\Http\Requests\UpdateItem;

class ItemController extends Controller
{
	public function index()
    {
        $items = Item::orderBy('id','DESC')->paginate(5);
        return view('public.pages.item.index', compact('items'));
       	
    }

    public function show(Item $item)
    {
        
    }

    public function create()
    {
      return view('public.pages.item.create');   
        
    }

    public function store(StoreItem $request)
    {
         $item = Item::create($request->validated());
    }

    public function update(UpdateItem $request, Item $item)
    {
        $item->fill($request->validated())->save();
        return redirect()->back()->with('info','El rubro '.$item->description.' fue actualizado.');


    }

    public function destroy(Item $item)
    {
        $item->delete();
         return redirect()->route('items')->with('info','El Rurbo '.$item->description.' fue Eliminado !');
       
    }
    
}

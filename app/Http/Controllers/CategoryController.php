<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Requests\StoreCategory;
use App\Http\Requests\UpdateCategory;

class CategoryController extends Controller
{
    public function index()
    {
        $categorias = Category::orderBy('id','DESC')->get();
        return view('public.pages.category.index', compact('categorias'));
       	
    }

    public function show(Category $category)
    {
        
    }

    public function store(StoreCategory $request)
    {
         $category = Category::create($request->validated());
    }

    public function update(UpdateCategory $request, Category $category)
    {
        $category->fill($request->validated())->save();

    }

    public function destroy(Category $category)
    {
        $category->delete();
       
    }
}

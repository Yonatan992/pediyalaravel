<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCategory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'item_is'     => 'required|exists:items,id',
            'description' => 'required|string|unique:categories,description|max:100,'.$this->category->id,
            'image'  => 'required|image|max:2048',
        ];
    }
}

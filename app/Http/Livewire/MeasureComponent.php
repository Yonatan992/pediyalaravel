<?php

namespace App\Http\Livewire;

use App\Models\Measure;
use Livewire\Component;
use App\Models\Category;
use Livewire\WithPagination;

class MeasureComponent extends Component
{
	use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $perPage = 5;
    public $sortField = 'description';
    public $sortAsc = true;

    public $search;

	public $description ,$categoria, $selected_id;
    public $updateMode = false;

    protected $messages = [
        "description.required" => "Por favor ingrese una descripcion",
        "description.unique" => "El campo descripcion ya esta en uso"

    ];

     public function updatingSearch()
    {
        $this->resetPage();
    }

    public function sortBy ($field)
    {
        if ($this->sortField == $field) {
            $this->sortAsc = ! $this->sortAsc;
        }else
        {
            $this->sortAsc =true;
        }

        $this->sortField = $field;
    }

    public function hydrate()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function render()
    {
        $search = '%'.$this->search.'%';
        return view('livewire.measures.measure-component', [
            'medidas' => Measure::whereHas('category', function ($query) use ($search) {
                $query->where('description', 'like', $search);})
                ->orderBy($this->sortField, $this->sortAsc ? 'ASC' : 'DESC')
                ->OrWhere('description','like', $search)->paginate($this->perPage),
            'categorias' => Category::all(),
        ]);
    }


    private function resetInput()
    {
    	$this->categoria = null;
        $this->description = null;
    }

    public function cancel()
    {
        $this->updateMode = false;
        $this->resetInput();
    }

    public function store()
    {
        $this->validate([
        	'categoria'     => 'required|exists:categories,id',
            'description' 	=> 'required|string|unique:measures,description|max:100',
        ]);
        //$img = $this->imagen->store('img/categorias');

        Measure::create([
        	'category_id'   => $this->categoria,
            'description' 	=> $this->description,
        ]);

        $this->resetInput();
        $this->emit('measureStore'); // Close model to using to jquery
        $this->emit('alertStore', ['type' => 'success', 'message' => 'La Medida se registro con éxito.']);  
    }


    public function edit($id)
    {
        $medida = Measure::findOrFail($id);
        $this->selected_id  = $id;
        $this->categoria    = $medida->category_id;
        $this->description  = $medida->description;
    }

    public function update()
    {
        $medida = Measure::findOrFail($this->selected_id);

        $this->validate([
            'categoria'   => 'required|exists:categories,id',
            'description' => ['required','string','max:100','unique:measures,description,'.$medida->id],
        ]);
        
        $medida->update([
            'category_id'   => $this->categoria,
            'description'   => $this->description,
        ]);

        session()->flash('message', 'El registro '.$medida->description.' se actualizo con éxito.');

        $this->resetInput();
        $this->emit('measureUpdate'); // Close model to using to jquery
        $this->emit('alertUpdate', ['type' => 'info', 'message' => 'El registro '.$medida->description. ' se modifico con éxito.']);
    }


    public function destroy($id)
    {
        $medida = Measure::findOrFail($id);
        $medida->delete();
        $this->emit('alertDestroy', ['type' => 'error', 'message' => 'El registro '.$medida->description. ' se elimino con éxito.']);
        
    }
}

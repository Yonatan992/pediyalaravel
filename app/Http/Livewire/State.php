<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;

use App\Models\State as Estado;
use App\Http\Requests\StoreState;

class State extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $perPage = 5;
    public $sortField = 'description';
    public $sortAsc = true;
    
	public $description ,$selected_id;
    public $updateMode = false;
    public $search;

    protected $messages = [
        "description.required"  => "Por favor ingrese una descripcion",
        "description.unique"    => "El campo descripcion ya esta en uso",
        "description.max"    =>"El campo descripcion debe contener 10 caracteres como máximo."
    ];

     public function updatingSearch()
    {
        $this->resetPage();
    }

    public function sortBy ($field)
    {
        if ($this->sortField == $field) {
            $this->sortAsc = ! $this->sortAsc;
        }else
        {
            $this->sortAsc =true;
        }

        $this->sortField = $field;
    }
    
     public function hydrate()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }


    public function render()
    {
        $search = '%'.$this->search.'%';
        return view('livewire.states.index', [
            'estados' => Estado::orderBy($this->sortField, $this->sortAsc ? 'ASC' : 'DESC')->where('description','like', $search)->paginate($this->perPage)
        ]);
    }


    private function resetInput()
    {
        $this->description = null;
    }

    public function cancel()
    {
        $this->updateMode = false;
        $this->resetInput();
    }

    public function store()
    {
        $this->validate([
            'description' => 'required|string|unique:states,description|max:100',
        ]);

        Estado::create([
            'description' => $this->description,
        ]);

        $this->resetInput();
        $this->emit('stateStore'); // Close model to using to jquery
        $this->emit('alertStore', ['type' => 'success', 'message' => 'El estado se registro con éxito.']);
    }

    public function edit($id)
    {
        $estado = Estado::findOrFail($id);
        $this->selected_id = $id;
        $this->description = $estado->description;
    
        
    }

    public function update()
    {
        $estado = Estado::findOrFail($this->selected_id);

        $this->validate([
            'description' => ['required','string','max:100','unique:states,description,'.$estado->id]
        ]);
        
        if ($estado) {
            $estado->update([
                'description' => $this->description,
            ]);

            $this->resetInput();
            $this->emit('stateUpdate');
            $this->emit('alertUpdate', ['type' => 'info', 'message' => 'El registro '.$estado->description. ' se modifico con éxito.']);
        }
        
    }

    public function destroy($id)
    {
        $estado = Estado::findOrFail($id);
        $estado->delete();
        $this->emit('alertDestroy', ['type' => 'error', 'message' => 'El registro '.$estado->description. ' se elimino con éxito.']);
        
    }
}

<?php

namespace App\Http\Livewire;

use Image;
use App\Models\Item;
use Livewire\Component;
use App\Models\Business;
use App\Models\Location;
use App\Models\Province;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Mail;
use App\Mail\NotificacionConfirmacion;
use Illuminate\Support\Facades\Validator;

class BusinnesComponent extends Component
{
    use WithPagination, WithFileUploads;
    protected $paginationTheme = 'bootstrap';
    public $perPage = 5;
    public $habilitado = 1, $deshabilitado =2,$estadoBan=3;
    public $sortField = 'name';
    public $sortAsc = true;
    public $search;

    public $avatar;

    protected $rules = [
        'avatar' => 'image|mimes:jpg,jpeg,png|max:1024',
    ];



    public $rubro,$estado = 2,$propietario,$nombreCo,$tel,$email,$direccion,$facebook,$instagram,$imagen,$imagenActual, $latitud, $longitud, $selected_id;
	public $locations = [];
    public $provinces = [];
    public $provinceId, $locationId;

    protected $messages = [
        "nombreCo.required" => "El campo nombre de comercio es obligatorio",
        "tel.required" => "El campo telefono es obligatorio",
    ];

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function sortBy ($field)
    {
        if ($this->sortField == $field) {
            $this->sortAsc = ! $this->sortAsc;
        }else
        {
            $this->sortAsc =true;
        }

        $this->sortField = $field;
    }

    public function states ($state)
    {
        $this->perPage = 5;
        $this->estadoBan =$state;
    }

    public function hydrate()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function updatedAvatar($value)
    {
        $validator = Validator::make(
            ['avatar' => $this->avatar],
            ['avatar' => $this->rules['avatar'],
            ]);

        if ($validator->fails()) {
            $this->reset('avatar');
            $this->setErrorBag($validator->getMessageBag());
        }
    }

    public function mount()
    {
        $this->refreshData();
        $this->states($this->estadoBan);
    }

    private function refreshData()
    {
        $this->provinces = Province::orderBy('description')->get();
        if (!empty($this->provinceId)) {

            $this->locations = Location::where('province_id', $this->provinceId)->get();
        }
    }

    public function render()
    {

    	$this->refreshData();
         $this->states($this->estadoBan);
        $search = '%'.$this->search.'%';
       return view('livewire.businnes.businnes-component', [
                'negocios' => Business::where('state_id','=',$this->estadoBan)

                ->whereHas('item', function ($query) use ($search){ 
                $query->orWhere('description', 'like', $search);})

                ->whereHas('location', function ($query) use ($search){ 
                $query->orWhere('description', 'like', $search);})

                ->where(function($query) use ($search){
                    $query->orWhere('owner','like', $search)
                    ->orWhere('name','like', $search);
                })
                
                ->orderBy($this->sortField, $this->sortAsc ? 'ASC' : 'DESC')
                ->paginate($this->perPage),
            'rubros' => Item::all(),
        ]);
    }

    private function resetInput()
    {
        $this->rubro        = null;
        $this->estado       = 2;
        $this->propietario  = null;
        $this->nombreCo     = null;
        $this->tel          = null;
        $this->email        = null;
        $this->direccion    = null;
        $this->latitud      = null;
        $this->longitud     = null;
        $this->facebook     = null;
        $this->instagram    = null;
        $this->imagen       = null;
        $this->imagenActual = null;
        $this->provinceId   = null;
        $this->locationId   = null;
        $this->locations    = [];
        $this->avatar       = null;
    }

    public function cancel()
    {
        $this->resetInput();
    }

    public function edit($id)
    {
        $negocio = Business::findOrFail($id);
        $this->selected_id  = $negocio->id;
        $this->rubro        = $negocio->item_id;
        $this->estado       = $negocio->state_id;
        $this->propietario  = $negocio->owner;
        $this->nombreCo     = $negocio->name;
        $this->tel          = $negocio->phone;
        $this->email        = $negocio->email;
        $this->direccion    = $negocio->address;
        $this->latitud      = $negocio->latitud;
        $this->longitud     = $negocio->longitud;
        $this->facebook     = $negocio->facebook;
        $this->instagram    = $negocio->instagram;
        $this->imagenActual = $negocio->image;
        $this->locationId   = $negocio->location_id;
        $this->provinceId   = $negocio->location->province->id;
    }

    public function update()
    {
        $negocio = Business::findOrFail($this->selected_id);
            $this->validate([
                'rubro'         => 'required|exists:items,id',
                'locationId'    => 'required|exists:locations,id',
                'estado'        => 'required|exists:states,id',
                'propietario'   => 'required|string|max:100',
                'nombreCo'      => 'required|string|max:100',
                'tel'           => 'required|string|max:11',
                'email'         => 'required|email:rfc,dns',
                'direccion'     => 'required|string',
                'latitud'       => 'nullable|max:20',
                'longitud'      => 'nullable|max:20',
                'avatar'        => 'nullable|mimes:jpg,jpeg,png|max:2048',
            ]);

        $url = $negocio->image;
        // Tratamiento de imagen
        if (!is_null($this->avatar)){

            $miImagen = public_path().$negocio->image;
            if (@getimagesize($miImagen)) {
                unlink($miImagen);
            }

            $imageResize = Image::make($this->avatar->getRealPath());
                $imageResize->resize(150, 150, function($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            $imageResize->orientate();
            $minuscula = strtolower($this->nombreCo);
            $nombreMinuscula = str_replace(" ", "-", $minuscula);
            $nombreArchivo = $nombreMinuscula . "." . $this->avatar->extension();
            $imageResize->save(public_path('imagenes/negocios/'.$nombreArchivo,50));
            $url = 'imagenes/negocios/'.$nombreArchivo;
        }
        
        $negocio->update([
            'item_id'       => $this->rubro,
            'location_id'   => $this->locationId,
            'state_id'      => $this->estado,
            'owner'         => $this->propietario,
            'name'          => $this->nombreCo,
            'phone'         => $this->tel,
            'email'         => $this->email,
            'address'       => $this->direccion,
            'latitud'       => $this->latitud,
            'longitud'      => $this->longitud,
            'image'         => $url,
        ]);
        $this->resetInput();
         $this->emit('businessUpdate'); // Close model to using to jquery
         $this->emit('alertUpdate', ['type' => 'success', 'message' => 'El Negocio '.$negocio->name. ' se modifico con éxito.']);
    }

    public function store()
    {

        $this->validate([
            'rubro'         => 'required|exists:items,id',
            'locationId'    => 'required|exists:locations,id',
            'estado'        => 'required|exists:states,id',
            'propietario'   => 'required|string|max:100',
            'nombreCo'      => 'required|string|max:100',
            'tel'           => 'required|string|max:11',
            'email'         => 'required|email:rfc,dns',
            'direccion'     => 'required|string',
        ]);

        Business::create([
            'item_id'       => $this->rubro,
            'location_id'   => $this->locationId,
            'state_id'      => $this->estado,
            'owner'         => $this->propietario,
            'name'          => $this->nombreCo,
            'phone'         => $this->tel,
            'email'         => $this->email,
            'address'       => $this->direccion,
        ]);

        session()->flash('message', 'Se registro con éxito su negocio.');
        $this->resetInput();
    }


    public function accept($id)
    {
        $negocio = Business::findOrFail($id);
        $negocio->update([
            'state_id'      => $this->habilitado,
        ]);
        $this->resetInput();
        retry(5, function() use($negocio) {
                Mail::to($negocio)->send(new NotificacionConfirmacion($negocio));
            }, 100);
         $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'El negocio: '.$negocio->name.' se ha aceptado. Ya puede crear usuarios.',
            'text'  => 'Se le enviará un correo electrónico informandolo. Atte. la administración',
        ]); 
    }

    public function deny($id)
    {
        $negocio = Business::findOrFail($id);
        $negocio->update([
            'state_id'      => $this->deshabilitado,
        ]);
        $this->resetInput();
        $this->emit('swal:modal', [
            'icon'  => 'error',
            'title' => 'Se ha deshabilitado a: '.$negocio->name.' Correctamente',
            'text'  => 'Se le enviará un correo electrónico informandolo.',
        ]); 
        
    }


}

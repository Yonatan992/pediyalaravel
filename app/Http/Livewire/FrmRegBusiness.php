<?php

namespace App\Http\Livewire;

use App\Models\Item;
use Livewire\Component;
use App\Models\Business;
use App\Models\Location;
use App\Models\Province;
use Livewire\WithPagination;
use App\Mail\NotificacionAdhesion;
use Illuminate\Support\Facades\Mail;

class FrmRegBusiness extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $rubro,$estado = 3,$propietario,$nombreCo,$tel,$email,$direccion,$facebook,$instagram,$imagen;
	public $locations = [];
    public $provinces = [];
    public $provinceId, $locationId;

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function hydrate()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function mount()
    {
        $this->refreshData();
    }

    private function refreshData()
    {
        $this->provinces = Province::orderBy('description')->get();
        if (!empty($this->provinceId)) {

            $this->locations = Location::where('province_id', $this->provinceId)->get();
        }
    }

    public function render()
    {

    	$this->refreshData();
       return view('livewire.regBusiness.frm-reg-business', [
            'items' => Item::orderBy('id','DESC')->get(),
        ]);
    }

    private function resetInput()
    {
        $this->rubro        = null;
        $this->estado       = 3;
        $this->propietario  = null;
        $this->nombreCo     = null;
        $this->tel          = null;
        $this->email        = null;
        $this->direccion    = null;
        $this->facebook     = null;
        $this->instagram    = null;
        $this->imagen       = null;
        $this->provinceId   = null;
        $this->locationId   = null;
        $this->locations    = [];
    }

    public function cancel()
    {
        $this->resetInput();
    }

    public function store()
    {

        $this->validate([
            'rubro'         => 'required|exists:items,id',
            'locationId'    => 'required|exists:locations,id',
            'estado'        => 'required|exists:states,id',
            'propietario'   => 'required|string|max:100',
            'nombreCo'      => 'required|string|max:100',
            'tel'           => 'required|string|max:11',
            'email'         => 'required|unique:businesses,email|email:rfc,dns',
            'direccion'     => 'required|string',
        ]);

        $business = Business::create([
            'item_id'       => $this->rubro,
            'location_id'   => $this->locationId,
            'state_id'      => $this->estado,
            'owner'         => $this->propietario,
            'name'          => $this->nombreCo,
            'phone'         => $this->tel,
            'email'         => $this->email,
            'address'       => $this->direccion,
        ]);
        $this->resetInput();
        retry(5, function() use($business) {
                Mail::to($business)->send(new NotificacionAdhesion($business));
            }, 100);
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Exelente!',
            'text'  => "Bienvenido!, se ha registrado su solicitud, dentro de las 24hsnp comunicaremos con usted.Le hemos enviado un correo al email proporcionado. Muchas gracias!",
        ]);  
    }
}

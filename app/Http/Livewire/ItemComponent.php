<?php

namespace App\Http\Livewire;

use App\Models\Item;
use Livewire\Component;
use Livewire\WithPagination;

class ItemComponent extends Component
{
	use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $perPage = 5;
    public $sortField = 'description';
    public $sortAsc = true;

    public $description ,$selected_id;
    public $updateMode = false;
    public $search;

    protected $messages = [
        "description.required"  => "Por favor ingrese una descripcion",
        "description.unique"    => "El campo descripcion ya esta en uso",
        "description.max"    =>"El campo descripcion debe contener 10 caracteres como máximo."
    ];

    public function sortBy ($field)
    {
        if ($this->sortField == $field) {
            $this->sortAsc = ! $this->sortAsc;
        }else
        {
            $this->sortAsc =true;
        }

        $this->sortField = $field;
    }

     public function updatingSearch()
    {
        $this->resetPage();
    }

    public function hydrate()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }



    public function render()
    {
        $search = '%'.$this->search.'%';
        return view('livewire.items.item-component', [
            'rubros' => Item::orderBy($this->sortField, $this->sortAsc ? 'ASC' : 'DESC')->where('description','like', $search)->paginate($this->perPage)
        ]);
    }

    private function resetInput()
    {
        $this->description = null;
    }

    public function cancel()
    {
        $this->updateMode = false;
        $this->resetInput();
    }

    public function store()
    {
        $this->validate([
            'description' => 'required|string|unique:items,description|max:100',
        ]);

        Item::create([
            'description' => $this->description,
        ]);

        $this->resetInput();
        $this->emit('itemStore'); // Close model to using to jquery
        $this->emit('alertStore', ['type' => 'success', 'message' => 'El Rubro se registro con éxito.']);  
    }

    public function edit($id)
    {
        $rubro = Item::findOrFail($id);
        $this->selected_id = $id;
        $this->description = $rubro->description;
    
        
    }

    public function update()
    {
        $rubro = Item::findOrFail($this->selected_id);

        $this->validate([
            'description' => ['required','string','max:100','unique:states,description,'.$rubro->id]
        ]);
        
        if ($rubro) {
            $rubro->update([
                'description' => $this->description,
            ]);

            $this->resetInput();
            $this->emit('itemUpdate');
            $this->emit('alertUpdate', ['type' => 'info', 'message' => 'El registro '.$rubro->description. ' se modifico con éxito.']);  
        }
        
    }

    public function destroy($id)
    {
        $rubro = Item::findOrFail($id);
        $descripcion = $rubro->description;
        $rubro->delete();
        $this->emit('alertDestroy', ['type' => 'error', 'message' => 'El registro '.$rubro->description. ' se elimino con éxito.']);
    }
}

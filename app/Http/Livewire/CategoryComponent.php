<?php

namespace App\Http\Livewire;
use App\Models\Category;
use App\Models\Item;
use App\Models\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Image;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Validator;

class CategoryComponent extends Component
{
	use WithPagination, WithFileUploads;
    protected $paginationTheme = 'bootstrap';
    public $perPage = 5;
    public $sortField = 'description';
    public $sortAsc = true;

	public $description ,$rubro , $imagen, $selected_id, $imagenActual;
    public $descriptionCat ,$category;
    public $updateMode = false;
    public $search, $searchsubCat;
    public $categoriesArray = [];
    public $itemsArray = [];
    public $itemId;

    public $avatar;

    protected $rules = [
        'avatar' => 'image|mimes:jpg,jpeg,png|max:1024',
    ];


    protected $messages = [
        "description.required" => "Por favor ingrese una descripcion",
        "description.unique" => "El campo descripcion ya esta en uso",
        "descriptionCat.required" => "Por favor ingrese una descripcion para la Subcategoria",
        "descriptionCat.unique" => "El campo descripcion ya esta en uso",
        "category.exists" => "Debe ingredar una categoria que exista",
        "category.required" => "Por favor Ingrese una Categoría",
        "avatar.required" => "Por favor Ingrese una Imagen (jpg, jpeg, png)",
    ];

     public function updatingSearch()
    {
        $this->resetPage();
    }

    public function sortBy ($field)
    {
        if ($this->sortField == $field) {
            $this->sortAsc = ! $this->sortAsc;
        }else
        {
            $this->sortAsc =true;
        }

        $this->sortField = $field;
    }


    public function hydrate()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function updatedAvatar($value)
    {
        $validator = Validator::make(
            ['avatar' => $this->avatar],
            ['avatar' => $this->rules['avatar'],
            ]);

        if ($validator->fails()) {
            $this->reset('avatar');
            $this->setErrorBag($validator->getMessageBag());
        }
    }

    public function mount()
    {
        $this->refreshData();
    }

    private function refreshData()
    {
        $this->itemsArray = Item::orderBy('description')->get();
        if (!empty($this->itemId)) {
            $this->categoriesArray = Category::where('item_id', $this->itemId)->get();
        }
    }

    public function render()
    {
        $search = '%'.$this->search.'%';
        $this->refreshData();
        return view('livewire.categories.category-component', [
            'categorias' => Category::whereHas('item', function ($query) use ($search) {
                $query->where('description', 'like', $search);})
                ->orderBy($this->sortField, $this->sortAsc ? 'ASC' : 'DESC')
                ->orWhere('description','like', $search)
                ->paginate($this->perPage),

            'rubros' => Item::all(),
        ]);
    }

    public function showSubCategory($id)
    {

    }


    private function resetInput()
    {
    	$this->rubro = null;
        $this->description = null;
        $this->imagen = null;
        $this->imagenActual = null;
        $this->avatar = null;
        $this->category = null;
        $this->descriptionCat = null;
    }

    public function cancel()
    {
        $this->updateMode = false;
        $this->resetInput();
    }

    public function store()
    {
        $this->validate([
        	'rubro'       	=> 'required|exists:items,id',
            'description' 	=> 'required|string|unique:categories,description|max:100',
            'avatar'  		=> 'required|image|mimes:jpg,jpeg,png|max:2048',
        ]);
        //$img = $this->imagen->store('img/categorias');

            //Tratamiento de imagen
            $imageResize = Image::make($this->avatar->getRealPath());
            $imageResize->resize(150, 150, function($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
            });
            $imageResize->orientate();
            $minuscula = strtolower($this->description);
            $nombreMinuscula = str_replace(" ", "-", $minuscula);
            $nombreArchivo = $nombreMinuscula . "." . $this->avatar->extension();
            $imageResize->save(public_path('imagenes/categorias/' . $nombreArchivo,50));
            $url = '/imagenes/categorias/'.$nombreArchivo;


        Category::create([
        	'item_id'     	=> $this->rubro,
            'description' 	=> $this->description,
            'image'			=> $url,
        ]);

        $this->resetInput();
        $this->emit('categoryStore'); // Close model to using to jquery
        $this->emit('alertStore', ['type' => 'success', 'message' => 'La Categoria se registro con éxito.']);  
    }

    public function edit($id)
    {
        $categoria = Category::findOrFail($id);
        $this->selected_id  = $id;
        $this->rubro        = $categoria->item_id;
        $this->description  = $categoria->description;
        $this->imagenActual = $categoria->image;
    }

    public function update()
    {
        $categoria = Category::findOrFail($this->selected_id);

        $this->validate([
            'rubro'       => 'required|exists:items,id',
            'description' => ['required','string','max:100','unique:categories,description,'.$categoria->id],
            'avatar'      => 'nullable|mimes:jpg,jpeg,png|max:2048',

        ]);

        $url = $categoria->image;
        // Tratamiento de imagen
        if (!is_null($this->avatar)){

            $miImagen = public_path().$categoria->image;
            if (@getimagesize($miImagen)) {
                unlink($miImagen);
            }

            $imageResize = Image::make($this->avatar->getRealPath());
                $imageResize->resize(150, 150, function($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            $imageResize->orientate();
            $minuscula = strtolower($this->description);
            $nombreMinuscula = str_replace(" ", "-", $minuscula);
            $nombreArchivo = $nombreMinuscula . "." . $this->avatar->extension();
            $imageResize->save(public_path('imagenes/categorias/'.$nombreArchivo,50));
            $url = 'imagenes/categorias/'.$nombreArchivo;
        }
        
        $categoria->update([
            'item_id'       => $this->rubro,
            'description'   => $this->description,
            'image'         => $url,
        ]);

        $this->resetInput();
        $this->emit('categoryUpdate'); // Close model to using to jquery
        $this->emit('alertUpdate', ['type' => 'info', 'message' => 'La categoria '.$categoria->description. ' se modifico con éxito.']);  
    }

    public function destroy($id)
    {
        $categoria = Category::findOrFail($id);
        $categoria->delete();
        $this->emit('alertDestroy', ['type' => 'error', 'message' => 'La categoria '.$categoria->description. ' se elimino con éxito.']);
        
    }

    // --- Logica para subcategorias ---

    public function indexSubCategory()
    {
        $searchSubCat = '%'.$this->searchSubCat.'%';
        return view('livewire.categories.category-component', [
            'categorias' => Subcategory::with('category:id,description')->orderBy('id','DESC')->where('description','like', $searchsubCat)->paginate(5),
        ]);
    }
    public function storeSubCategory()
    {
        $this->validate([
            'category'         => 'required|exists:categories,id',
            'descriptionCat'   => 'required|string|unique:categories,description|max:100',
        ]);

        SubCategory::create([
            'category_id'   => $this->category,
            'description'   => $this->descriptionCat,
        ]);

        session()->flash('message', 'Se registro con éxito una Subcategoría.');
        $this->resetInput();
        $this->emit('subCategoryStore'); // Close model to using to jquery
    }

}

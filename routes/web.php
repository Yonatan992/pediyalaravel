<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Rutas publicas
Route::view('/', 'public.pages.index')->name('index');
Route::view('/ingresar', 'public.pages.forms.login')->name('ingresar');
Route::view('/register-client', 'public.pages.forms.frm-reg-client')->name('register_client');
Route::get('/register-business', 'BusinnesController@createBusinnes')->name('register_business');
Route::view('/ubication', 'public.pages.map')->name('map');

//Cliente(index)
Route::view('/cliente', 'public.pages.client')->name('client');
//---------------------------------//

//Rutas admin y superadmin
Route::view('/root', 'root.dashboard.index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// rutas correspondistes a Paises
Route::get('provinces','ProvinceController@index')->name('provinces');
Route::get('province/{id}','ProvinceController@show')->name('province.show');
Route::post('province','ProvinceController@store')->name('province.store');
Route::put('province/{id}','ProvinceController@update')->name('province.update');
Route::delete('province/{id}','ProvinceController@destroy')->name('province.destroy');

// ** Livewire ** 
// rutas correspondistes a Estados
Route::view('states', 'public.pages.state.index')->name('states');
// rutas correspondistes a Categoria
Route::view('categories', 'public.pages.category.index')->name('categories');
// rutas correspondistes a Medidas
Route::view('measures', 'public.pages.measure.index')->name('measures');
// rutas correspondistes a Rubros
Route::view('items', 'public.pages.item.index')->name('items');

// rutas correspondistes a Negocios
Route::view('business', 'public.pages.business.index')->name('business');


// rutas correspondistes a Rubros
Route::get('items','ItemController@index')->name('items');
Route::get('item/{item}','ItemController@show')->name('item.show');
Route::get('items/create','ItemController@create')->name('item.create');
Route::post('item-store','ItemController@store')->name('item.store');
Route::put('item/update/{item}','ItemController@update')->name('item.update');
Route::delete('item/destroy/{item}','ItemController@destroy')->name('item.destroy');


// rutas correspondistes a Tipos de Comercios
Route::get('type-businesses','TypeBusinessController@index')->name('type-businesses');
Route::get('type-business/{id}','TypeBusinessController@show')->name('type-business.show');
Route::post('type-business','TypeBusinessController@store')->name('type-business.store');
Route::put('type-business/{id}','TypeBusinessController@update')->name('type-business.update');
Route::delete('type-business/{id}','TypeBusinessController@destroy')->name('type-business.destroy');

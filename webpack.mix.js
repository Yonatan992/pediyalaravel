const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/admin/root/root.js', 'public/js')
   .js('resources/js/admin/admin/admin.js', 'public/js')
   .js('resources/js/admin/share/share.js', 'public/js')
   .js('resources/js/public/public.js', 'public/js')
    .sass('resources/sass/admin/root/root.scss', 'public/css')
    .sass('resources/sass/admin/admin/admin.scss', 'public/css')
    .sass('resources/sass/admin/share/share.scss', 'public/css')
    .sass('resources/sass/public/public.scss', 'public/css')
    .combine('resources/plugin', 'public/js/plugin.js');

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('head_id')->unsigned();
            $table->foreign('head_id')->references('id')->on('head_orders');
            $table->bigInteger('lot_id')->unsigned();
            $table->foreign('lot_id')->references('id')->on('lots');
            $table->decimal('count', 15, 3);
            $table->decimal('price_list', 15, 2);
            $table->decimal('price_sale', 15, 2);
            $table->decimal('percent_discount', 15, 2);
            $table->decimal('subtotal', 15, 2);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_orders');
    }
}

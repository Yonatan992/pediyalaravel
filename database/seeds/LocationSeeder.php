<?php

use Illuminate\Database\Seeder;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     //    DB::statement('SET FOREIGN_KEY_CHECKS = 0;'); // Desactivamos la revision de claves foraneas
    	// DB::table('locations')->truncate();
     //    DB::statement('SET FOREIGN_KEY_CHECKS = 1;'); // Reactivamos la revision de claves foraneas

        DB::table('locations')->update([
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

      //   DB::table('locations')->insert([
      //       'province_id' => 1,
      //       'description' => 'Corrientes',
      //       'created_at' => date('Y-m-d H:i:s'),
    		// 'updated_at' => date('Y-m-d H:i:s'),
      //   ]);

      //   DB::table('locations')->insert([
      //       'province_id' => 1,
      //       'description' => 'Mercedes',
      //       'created_at' => date('Y-m-d H:i:s'),
    		// 'updated_at' => date('Y-m-d H:i:s'),
      //   ]);

      //   DB::table('locations')->insert([
      //       'province_id' => 1,
      //       'description' => 'Itati',
      //       'created_at' => date('Y-m-d H:i:s'),
    		// 'updated_at' => date('Y-m-d H:i:s'),
      //   ]);

      //    DB::table('locations')->insert([
      //       'province_id' => 1,
      //       'description' => 'Empedrado',
      //       'created_at' => date('Y-m-d H:i:s'),
    		// 'updated_at' => date('Y-m-d H:i:s'),
      //   ]);

      //    DB::table('locations')->insert([
      //       'province_id' => 2,
      //       'description' => 'Resistecia',
      //       'created_at' => date('Y-m-d H:i:s'),
    		// 'updated_at' => date('Y-m-d H:i:s'),
      //   ]);

      //    DB::table('locations')->insert([
      //       'province_id' => 2,
      //       'description' => 'Sáenz Peña',
      //       'created_at' => date('Y-m-d H:i:s'),
    		// 'updated_at' => date('Y-m-d H:i:s'),
      //   ]);

    }
}

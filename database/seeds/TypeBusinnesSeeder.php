<?php

use Illuminate\Database\Seeder;

class TypeBusinnesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;'); // Desactivamos la revision de claves foraneas
    	DB::table('type_businesses')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;'); // Reactivamos la revision de claves foraneas

        DB::table('type_businesses')->insert([
            'description' => 'Ferreteria',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

        DB::table('type_businesses')->insert([
            'description' => 'Comida',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

        DB::table('type_businesses')->insert([
            'description' => 'Cerveceria',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

        DB::table('type_businesses')->insert([
            'description' => 'Panaderia',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

         DB::table('type_businesses')->insert([
            'description' => 'Farmacia',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }

}

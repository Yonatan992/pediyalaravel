<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	$this->call(TypeBusinnesSeeder::class);
    	$this->call(ProvinceCSVSeeder::class);
    	$this->call(ItemSeeder::class);
        $this->call(StateSeeder::class);
    	$this->call(CategorySeeder::class);
    	$this->call(LocationCSVSeeder::class);
        $this->call(BusinnesSeeder::class);
        // $this->call(UserSeeder::class);
    }
}

<?php

use Illuminate\Database\Seeder;

class BusinnesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;'); // Desactivamos la revision de claves foraneas
    	DB::table('businesses')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;'); // Reactivamos la revision de claves foraneas

        DB::table('businesses')->insert([
            'item_id' 			=> 1,
            'location_id' 		=> 1,
            'state_id'   		=>1,
            'name'				=>'Los Olmos',
            'owner'				=>'Cosme Fulanito',
            'phone'				=>'38574096',
            'email'				=>'olmos@gmail.com',
            'email_verified_at'	=>now(),
            'address'			=>'Los Olmos',
            'facebook'			=>'ur-facebook',
            'instagram'			=>'url-instagram',
            'image'				=>'url-imagen',
            'created_at' 		=> date('Y-m-d H:i:s'),
    		'updated_at' 		=> date('Y-m-d H:i:s'),
        ]);

        DB::table('businesses')->insert([
            'item_id' 			=> 2,
            'location_id' 		=> 1,
            'state_id'   		=>1,
            'name'				=>'Cerveceria del centro',
            'owner'				=>'Cosme Fulanito',
            'phone'				=>'38574096',
            'email'				=>'cerveceria@gmail.com',
            'email_verified_at'	=>now(),
            'address'			=>'Los Olmos',
            'facebook'			=>'ur-facebook',
            'instagram'			=>'url-instagram',
            'image'				=>'url-imagen',
            'created_at' 		=> date('Y-m-d H:i:s'),
    		'updated_at' 		=> date('Y-m-d H:i:s'),
        ]);

        DB::table('businesses')->insert([
            'item_id' 			=> 3,
            'location_id' 		=> 1,
            'state_id'   		=>1,
            'name'				=>'Farmar',
            'owner'				=>'Cosme Fulanito',
            'phone'				=>'38574096',
            'email'				=>'farmar@gmail.com',
            'email_verified_at'	=>now(),
            'address'			=>'Los Olmos',
            'facebook'			=>'ur-facebook',
            'instagram'			=>'url-instagram',
            'image'				=>'url-imagen',
            'created_at' 		=> date('Y-m-d H:i:s'),
    		'updated_at' 		=> date('Y-m-d H:i:s'),
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class ProvinceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     //    DB::statement('SET FOREIGN_KEY_CHECKS = 0;'); // Desactivamos la revision de claves foraneas
    	// DB::table('provinces')->truncate();
     //    DB::statement('SET FOREIGN_KEY_CHECKS = 1;'); // Reactivamos la revision de claves foraneas


        //para actualizar su fecha de creacion y update y evitar el null
        
        DB::table('provinces')->update([
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

      //   DB::table('provinces')->insert([
      //       'description' => 'Corrientess',
      //       'created_at' => date('Y-m-d H:i:s'),
    		// 'updated_at' => date('Y-m-d H:i:s'),
      //   ]);

      //   DB::table('provinces')->insert([
      //       'description' => 'Chacoo',
      //       'created_at' => date('Y-m-d H:i:s'),
    		// 'updated_at' => date('Y-m-d H:i:s'),
      //   ]);

      //   DB::table('provinces')->insert([
      //       'description' => 'Formosaa',
      //       'created_at' => date('Y-m-d H:i:s'),
    		// 'updated_at' => date('Y-m-d H:i:s'),
      //   ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('categories')->insert([
            'item_id' => 1,
            'description' => 'Hamburguesas',
            'image'      => 'url-imagen',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

        DB::table('categories')->insert([
            'item_id' => 1,
            'description' => 'Pizzas',
            'image'      => 'url-imagen',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

        DB::table('categories')->insert([
            'item_id' => 1,
            'description' => 'Empanadas',
            'image'      => 'url-imagen',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

        DB::table('categories')->insert([
            'item_id' => 1,
            'description' => 'Lomitos',
            'image'      => 'url-imagen',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

         DB::table('categories')->insert([
            'item_id' => 2,
            'description' => 'Quilmes',
            'image'      => 'url-imagen',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

         DB::table('categories')->insert([
            'item_id' => 2,
            'description' => 'Sol',
            'image'      => 'url-imagen',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

         DB::table('categories')->insert([
            'item_id' => 2,
            'description' => 'Andes',
            'image'      => 'url-imagen',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

         DB::table('categories')->insert([
            'item_id' => 3,
            'description' => 'Medicamentos',
            'image'      => 'url-imagen',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        DB::table('categories')->insert([
            'item_id' => 3,
            'description' => 'Higiene',
            'image'      => 'url-imagen',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }
}

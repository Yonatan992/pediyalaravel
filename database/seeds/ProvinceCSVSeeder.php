<?php

use Illuminate\Database\Seeder;
use Flynsarmy\CsvSeeder\CsvSeeder;

class ProvinceCSVSeeder extends CsvSeeder
{
	public function __construct()
	{
		$this->table = 'provinces';
		$this->filename = base_path().'/database/seeds/csvs/provincias.csv';
	}
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Recommended when importing larger CSVs
		//DB::disableQueryLog();

		 DB::statement('SET FOREIGN_KEY_CHECKS = 0;'); // Desactivamos la revision de claves foraneas
    	DB::table($this->table)->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;'); // Reactivamos la revision de claves foraneas

		parent::run();

		 DB::table($this->table)->update([
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }
}

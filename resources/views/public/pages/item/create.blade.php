@extends('root.master')
@section('title', 'Rubros')
@section('content')
<div class="panel-header bg-primary-gradient">
  <div class="page-inner py-5">
    <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
      <div>
        <h2 class="text-white pb-2 fw-bold">Registrar Rubros</h2>
      </div>
    </div>
  </div>
</div>
<br>

<div class="container">
      	@if(Session::has('msj'))
          <div class="alert alert-success" role="alert">
            {{ Session::get('msj') }}
          </div>
        @endif

<div class="card">
    <div class="row">
        <div class="col-md-12">
            <div class="well well-sm">
                <form class="form-horizontal" action="{{route('item.store')}}" method="POST">
                  @csrf
                        <div class="form-group">
                           <label>Descripcion</label>
                            <div class="col-md-8">
                                <textarea class="form-control" name="description" placeholder="Ingrese aqui una descripcion del Rubro" rows="7">{{old('description')}}</textarea>
                                <span class="text-danger">{{ $errors->first('description') }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 text-center"><a type=button href="{{ url('items') }}"  class="btn btn-outline-secondary btn-lg"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i>&nbsp;&nbsp;Volver</a>&nbsp;&nbsp;
                                <button type="submit" class="btn btn-outline-primary btn-lg">Guardar&nbsp;&nbsp;<i class="fa fa-check-square-o" aria-hidden="true"></i></button>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
  </div>
</div>
@endsection
@extends('root.master')
@section('content')
<div class="panel-header bg-primary-gradient">
	<div class="page-inner py-3">
		<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
			<div>
				<h2 class="text-white pb-2 fw-bold">Rubros</h2>
			</div>
		</div>
	</div>
</div>
<br>

<div class="flex justify-center">
 	@livewire('item-component')
 </div>
@endsection 

@push('scripts')
    <script type="text/javascript">
        window.livewire.on('itemStore', () => {
            $('#itemModal').modal('hide');
        });

        window.livewire.on('itemUpdate', () => {
        $('#updateModal').modal('hide');
    });
        window.livewire.on('swal:alert', data => {
            swal(data.icon, data.title, data.timeout)
        });

            window.livewire.on('alertStore', param => {
        toastr[param['type']](param['message']);
    });

        window.livewire.on('alertUpdate', param => {
            toastr[param['type']](param['message']);
    });

        window.livewire.on('alertDestroy', param => {
            toastr[param['type']](param['message']);
    }); 
    </script>
@endpush
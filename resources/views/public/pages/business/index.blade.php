@extends('root.master')
@section('content')
<div class="panel-header bg-primary-gradient">
	<div class="page-inner py-3">
		<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
			<div>
				<h2 class="text-white pb-2 fw-bold">Negocios</h2>
			</div>
		</div>
	</div>
</div>
<br>

<div class="flex justify-center">
 	@livewire('businnes-component')
</div>
@endsection 

@push('scripts')

    <script type="text/javascript">
        window.livewire.on('businessUpdate', () => {
        $('#updateModal').modal('hide');});

       	window.livewire.on('alertDeny', param => {
        toastr[param['type']](param['message']);
	    });

	    window.livewire.on('alertAccept', param => {
        toastr[param['type']](param['message']);
	    });

        window.livewire.on('alertUpdate', param => {
            toastr[param['type']](param['message']);
    	});

    </script>
     <script>
        const SwalModal = (icon, title, html) => {
            Swal.fire({
                icon,
                title,
                html
            })
        }

        const SwalConfirm = (icon, title, html, confirmButtonText, method, params, callback) => {
            Swal.fire({
                icon,
                title,
                html,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText,
                reverseButtons: true,
            }).then(result => {
                if (result.value) {
                    return livewire.emit(method, params)
                }

                if (callback) {
                    return livewire.emit(callback)
                }
            })
        }

        const SwalAlert = (icon, title, timeout = 7000) => {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: timeout,
                onOpen: toast => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })

            Toast.fire({
                icon,
                title
            })
        }

        document.addEventListener('DOMContentLoaded', () => { 
            this.livewire.on('swal:modal', data => {
                SwalModal(data.icon, data.title, data.text)
            })

            this.livewire.on('swal:confirm', data => {
                SwalConfirm(data.icon, data.title, data.text, data.confirmText, data.method, data.params, data.callback)
            })

            this.livewire.on('swal:alert', data => {
                SwalAlert(data.icon, data.title, data.timeout)
            }) 
        })
    </script>
@endpush

@extends('public.layout')
@section('content')
<section class="cuerpo">

    <section class="section-portada">

        <div class="select-neg">

            <div class="container">
                <input type="text" placeholder="Busca tu negocio!">
                <div class="search"></div>
            </div>
        </div>
        
    </section>

    <section class="section-rubros">    

        <div id="rubros">
            
        </div>

    </section>
    

    <section class="section-adherite" id="section-adherite">
        <div class="grid-adherite">
            <div class="img-fondo">
                <img src="{{ asset('files/public/img/imgVarios/adherite.jpeg') }}">
            </div>
            <div class="title">
                <p style="text-align: center">VENDÉ CON NOSOTROS E IMPULSÁ TU NEGOCIO! <a href="{{ route('register_business') }}">ADHERITE</a></p>
            </div>
        </div>
    </section>
</section>

@endsection
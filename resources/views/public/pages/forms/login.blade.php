@extends('public.layout')
@section('content')

<section class="contenedor-login">
    <div class="box">
    <h2>Acceso</h2>
    <form id="login" action="signinAdmin">
        <div class="inputBox">
        <input type="text" name="usuario" autofocus="autofocus" required>
        <label for="usuario" class="label-name">
            <span class="content-name">Nombre de usuario</span>
        </label>
        </div>
        <div class="inputBox">
        <input type="password" name="pass" autocomplete="off" required>
        <label for="pass" class="label-name">
            <span class="content-name">Contraseña</span>
        </label>
        </div>
        <br>
        <button type="submit" id="btnLogin" class="inputButton"><span id="nom">Acceder</span></button>
    </form>
    </div>
</section>

@endsection
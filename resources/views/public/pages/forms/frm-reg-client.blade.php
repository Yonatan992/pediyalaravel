@extends('public.layout')
@section('content')
<section class="contenedor-registro">
  <div class="box">
    <h2>Registro Cliente</h2>
    <form id="registro" action="signupCliente">
      <div class="inputBox">
        <input type="text" name="email" autofocus="autofocus" required>
        <label for="email" class="label-name">
          <span class="content-name">Email</span>
        </label>
      </div>
      <div class="inputBox">
        <input type="password" name="pass" required>
        <label for="contrasenia"class="label-name">
          <span class="content-name">Contraseña</span>
        </label>
      </div>
      <div class="inputBox">
        <select name="provincia" id="prov">
          <option selected disabled value="0">Seleccione una...</option>
          
        </select>
        <label for="provincia" class="label-name">
          <span class="content-name">Provincia</span>
        </label>
      </div>
      <div class="inputBox">
        <select name="localidad" id="loc">
          <option selected disabled value="0">Seleccione una...</option>
        </select>
        <label for="localidad" class="label-name">
          <span class="content-name">Localidad</span>
        </label>
      </div>
      <br>
      <button type="submit" id="btnRegistro" class="inputButton"><span id="nom">Registrar</span></button>
    </form>
  </div>
</section>
@endsection
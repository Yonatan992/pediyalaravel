<div class="menu_bar">
    <a href="#" class="btn-menu"><span class="icon-menu"></span>Menu</a>
</div>

<nav>
    <ul>

        <li><a href="#">LOGO EyP</a></li>
        <li><a href="{{ route('index') }}"><span class="fa fa-home"></span>Inicio</a></li>
        <li><a href="#" id="linkModal">¿Cómo comprar?</a></li>
        
            <li class="ingresar"><a href="{{ route('login') }}"><span class="fa fa-user"></span>Ingresar</a></li>
            <li class="ingresar"><a href="{{ route('register_client') }}"><span class="fa fa-folder-open"></span>Registrate</a></li>
    
        <li class="ingresar"><a href="#"><span class="fa fa-shopping-cart"></span>Mi carrito</a></li>
    </ul>
</nav>


<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<html>
    <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie-edge">
  
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.css" />

    @livewireStyles
    <title>Entra y Pedi</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/public.css') }}">

    </head>
      
  <body>

    <div class="contenedor">

        <!-- HEADER -->
        <header class="header">
          @include('public.components.header')
        </header>
        
        <main class="contenido">
          @yield('content')
        </main>
      
        <!-- FOOTER -->
        <footer class="footer">
          @include('public.components.footer')
        </footer>

    </div>

    @include('public.modals.mdl-como-comprar')
    <script src="{{ asset('js/public.js') }}"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAY_omp-yrvfd8xJYYMi7lvmuPU9RT3SBE&callback=initMap&libraries=places&v=weekly"
      defer
    ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    @livewireScripts
    @stack('scripts')
  </body>
</html>

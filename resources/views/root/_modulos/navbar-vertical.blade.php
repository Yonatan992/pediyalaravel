<div class="sidebar sidebar-style-2">

	<div class="sidebar-wrapper scrollbar scrollbar-inner">
		<div class="sidebar-content">
			<div class="user">
				<div class="avatar-sm float-left mr-2">
					<img src="{{asset("img/logos/avatar5.png")}}"  alt="..." class="avatar-img rounded-circle">
				</div>
				<div class="info">
					<a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
						<span>
							Hizrian
							<span class="user-level">Administrator</span>
							<span class="caret"></span>
						</span>
					</a>
					<div class="clearfix"></div>

					<div class="collapse in" id="collapseExample">
						<ul class="nav">
							<li>
								<a href="#profile">
									<span class="link-collapse">My Profile</span>
								</a>
							</li>
							<li>
								<a href="#edit">
									<span class="link-collapse">Edit Profile</span>
								</a>
							</li>
							<li>
								<a href="#settings">
									<span class="link-collapse">Settings</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<ul class="nav nav-tabs">
				<li class="nav-item active">
					<a href="/" aria-expanded="false">
						<i class="fas fa-home"></i>
						<p>Dashboard</p>
					</a>
				</li>
				<li class="nav-section">
					<span class="sidebar-mini-icon">
						<i class="fa fa-ellipsis-h"></i>
					</span>
					<h4 class="text-section">Components</h4>
				</li>
				<li class="nav-item">
					<a data-toggle="collapse" href="#base">
						<i class="fas fa-layer-group"></i>
						<p>Administracion</p>
						<span class="caret"></span>
					</a>
					<div class="collapse" id="base">
						<ul class="nav nav-collapse">
							<li>
								<a href="../components/avatars.html">
									<span class="sub-item">Usuarios</span>
								</a>
							</li>
							<li>
								<a href="../components/buttons.html">
									<span class="sub-item">Buttons</span>
								</a>
							</li>
						</ul>
					</div>
				</li>
				<li class="nav-item {{ Request::is ('business') ? 'active' : null }}">
					<a data-toggle="collapse" href="#negocios">
						<i class="fas fa-layer-group"></i>
						<p>Negocios</p>
						<span class="caret"></span>
					</a>
					<div class="collapse" id="negocios">
						<ul class="nav nav-collapse">
							<li class="{{ Request::segment(1) === 'business' ? 'active' : null }}">
								<a href="{{ route('business') }}" class="nav-link">
									<span class="sub-item">Negocios</span>
								</a>
							</li>
							<li>
								<a href="../components/buttons.html">
									<span class="sub-item">Buttons</span>
								</a>
							</li>
						</ul>
					</div>
				</li>

				<li class="nav-item {{( (Request::is('items') || Request::is ('categories') || Request::is('measures') || Request::is ('states') ) ? 'active' : null )}}">
					<a data-toggle="collapse" data-target="#datos">
						<i class="fas fa-layer-group"></i>
						<p>Datos</p>
						<span class="caret"></span>
					</a>
					<div class="collapse in"  id="datos">
						<ul class="nav nav-tabs">
							<li class="{{ Request::segment(1) === 'items' ? 'active' : null }}">
								<a href="{{ route('items') }}" class="nav-link">
									<span class="sub-item">Rubros</span>
								</a>
							</li>

							<li class="{{ Request::segment(1) === 'categories' ? 'active' : null }}">
								<a href="{{ route('categories') }}" class="nav-link">
									<span class="sub-item">Categorias</span>
								</a>
							</li>
							<li class="{{ Request::segment(1) === 'measures' ? 'active' : null }}">
								<a href="{{ route('measures') }}" class="nav-link">
									<span class="sub-item">Medidas</span>
								</a>
							</li>
							<li class="{{ Request::segment(1) === 'states' ? 'active' : null }}">
								<a href="{{ route('states') }}" class="nav-link">
									<span class="sub-item">Estados</span>
								</a>
							</li>
						</ul>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div>

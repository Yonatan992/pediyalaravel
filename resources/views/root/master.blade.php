<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">

	<link rel="stylesheet" href="{{ asset('css/share.css') }}">
	<link rel="stylesheet" href="{{ asset('css/atlantis.min.css') }}">
	<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" integrity="sha512-vKMx8UnXk60zUwyUnUPM3HbQo8QfmNx7+ltw8Pm5zLusl1XIfwcxo8DbWCqMGKaWeNxWA8yrx5v3SaVpMvR3CA==" crossorigin="anonymous" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.css" />
	@livewireStyles
	<title>Entra y Pedi - @yield('title')</title>
</head>

<body>
	<div class="wrapper">
		{{-- NavBar Vertical --}}
		@include('root._modulos.navbar-horizontal')

		<!-- Sidebar -->
		@include('root._modulos.navbar-vertical')

		<div class="main-panel">
			<div class="content">
				@yield('content')
			</div>

			@include('root._modulos.footer')
		</div>

	</div>

	<script src="{{ asset('js/share.js') }}"></script>
	<script src="{{ asset('js/plugin.js') }}"></script>
	<script>
		WebFont.load({
		google: {"families":["Lato:300,400,700,900"]},
		custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ["{{ asset('css/fonts.min.css') }}"]},
		active: function() {
			sessionStorage.fonts = true;
		}
	});
	</script>
	<script src="{{ asset('js/atlantis.min.js') }}"></script>
	<script>
			$("nav nav-tabs").on("click", function(){
		   	$("datos").find(".active").removeClass("active");
		   	$(this).addClass("active");
	});
	</script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	@livewireScripts
	@stack('scripts')

</body>

</html>

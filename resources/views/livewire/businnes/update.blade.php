<!-- Modal -->
<div wire:ignore.self class="modal fade" id="updateModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-info">
				<h5 class="modal-title" id="exampleModalLabel">Verificar Negocio</h5>
				<button type="button" wire:click.prevent="cancel()" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					<form>
						<div class="container">
							<div class="card">
								<input type="hidden" wire:model="selected_id">
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											@if ($avatar)
											<br>
												<img src="{{ $avatar->temporaryUrl() }}" class="rounded mx-auto d-block" width="150" height="150" >

											@elseif ($imagenActual) 
												<img src="{{ $imagenActual}}" class="rounded mx-auto d-block" width="150" height="150" >
						
											@else 
												<img src="{{ asset('img/logos/upload.png') }}" class="rounded mx-auto d-block" width="150" height="150" >
											@endif
											<br>
											<div class="custom-file">
												<input type="file" wire:model="avatar" class="custom-file-input" id="customFileLang">
												<label class="custom-file-label" for="customFileLang">Subir Imagen</label>
											</div>
											@error('avatar') <span class="text-danger error">* {{ $message }}</span>@enderror
										</div>

										<div class="form-group">
											<label >Nombre de comercio</label>
											<br>
											<input type="text" wire:model="nombreCo" class="form-control input-sm"  placeholder="Descripcion">
											@error('nombreCo') <span class="text-danger error">* {{ $message }}</span>@enderror
										</div>


										<div class="form-group">
											<label >Rubro</label>
											<br>
											<select wire:model="rubro" class="custom-select">
												@foreach($rubros as $rubro)
												<option value="{{ $rubro->id }}">{{ $rubro->description }}</option>
												@endforeach
											</select>
											@error('rubro') <span class="text-danger error">* {{ $message }}</span>@enderror
										</div>

										<div class="form-group">
											<label >Latitud</label>
											<br>
											<input type="text" wire:model="latitud" class="form-control input-sm"  placeholder="Latitud">
											@error('latitud') <span class="text-danger error">* {{ $message }}</span>@enderror
										</div>
										<div class="form-group">
											<label >Longitud</label>
											<br>
											<input type="text" wire:model="longitud" class="form-control input-sm"  placeholder="Longitud">
											@error('longitud') <span class="text-danger error">* {{ $message }}</span>@enderror
										</div>

										
									</div>

									<div class="col-6">

										<div class="form-group row">
											<div class="form-group col-md-6">
												<label >Provincia</label>
												<br>
												<select wire:model="provinceId" class="custom-select">
													@foreach($provinces as $province)
													<option value="{{ $province->id }}">{{ $province->description }}</option>
													@endforeach
												</select>
												@error('locationId') <span class="text-danger error">* {{ $message }}</span>@enderror
											</div>
											
												
											@if(count($this->locations) > 0)
											<div class="form-group col-md-6">
												<label >Localidad</label>
												<br>
												<select wire:model="locationId" class="custom-select">
													
													@foreach($locations as $localidad)
													<option value="{{ $localidad->id }}">{{ $localidad->description }}</option>
													@endforeach
												</select>
												@error('locationId') <span class="text-danger error">* {{ $message }}</span>@enderror
											</div>
											@endif
										</div>
										<div class="form-group">
											<label >Dirección del Comercio</label>
											<br>
											<input type="text" wire:model="direccion" class="form-control input-sm"  placeholder="Direccion de comercio">
											@error('direccion') <span class="text-danger error">* {{ $message }}</span>@enderror
										</div>
										<div class="form-group">
											<label >Telefono</label>
											<br>
											<input type="text" wire:model="tel" class="form-control input-sm"  placeholder="telefono">
											@error('tel') <span class="text-danger error">* {{ $message }}</span>@enderror
										</div>
										<div class="form-group">
											<label >Propietario</label>
											<br>
											<input type="text" wire:model="propietario" class="form-control input-sm"  placeholder="Propietario">
											@error('propietario') <span class="text-danger error">* {{ $message }}</span>@enderror
										</div>

										<div class="form-group">
											<label >Correo</label>
											<br>
											<input type="text" wire:model="email" class="form-control input-sm"  placeholder="Email">
											@error('email') <span class="text-danger error">* {{ $message }}</span>@enderror
										</div>

									</div>

								</div>

							</div>
						</div>
					</form>
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" wire:click.prevent="update()" class="btn btn-primary close-modal"><i class="fas fa-save"> Verificar</i></button>
				<button type="button" wire:click.prevent="cancel()" class="btn btn-danger close-btn" data-dismiss="modal"><i class="fas fa-times-circle"> Cerrar</i></button>

			</div>
		</div>
	</div>
</div>
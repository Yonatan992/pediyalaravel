<div class="container">
    @include('livewire.businnes.update')
    <div class="card p-2">
        @if(session()->has('message'))
        <br>
        <div class="alert alert-success">{{ session('message') }}</div>
        @endif
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="{{ $estadoBan === '3' ? 'nav-link active' : 'nav-link' }}" wire:click.prevent="states('3')"href="#">Pendientes</a>
            </li>
            <li class="nav-item" >
                <a class="{{ $estadoBan === '1' ? 'nav-link active' : 'nav-link' }}" wire:click.prevent="states('1')" href="#">Habilitados</a>
            </li>
             <li class="nav-item" >
                <a class="{{ $estadoBan === '2' ? 'nav-link active' : 'nav-link' }}" wire:click.prevent="states('2')" href="#">Deshabilitado</a>
            </li>
        </ul>
        
        <div class="table-responsive">
            <form class="form-inline d-flex float-right md-form form-sm mt-0">
                <i class="fas fa-search" aria-hidden="true"></i>
                <input class="form-control form-control-sm ml-3 w-75" style="margin: 10px" type="text" placeholder="Buscar" wire:model="search" aria-label="Search">
            </form>
            @if ($negocios->count())
            <br>
                    <div class="col form-inline p-2">
                        Mostrar &nbsp;
                        <select wire:model="perPage" class="form-control">
                            <option>5</option>
                            <option>10</option>
                            <option>15</option>
                        </select>
                        &nbsp; registros       
                    </div>
                    
                <table class="table table-striped table-bordered table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th><a class="text-light" wire:click.prevent="sortBy('name')" role="button" href="#">
                            Comercio&nbsp;
                            @include('livewire.categories._sort-icon',['field'=>'name'])
                            </a></th>
                            <th>Rubro</th>
                            <th>Dueño</th>
                            <th>Telefono</th>
                            <th>Ubicación</th>
                            <th>Ver/Modificar</th>
                            <th>Aceprtar/Rechazar</th>
                        </tr>
                    </thead>
                    <tbody align="center">
                        @foreach($negocios as $negocio)
                        <tr>
                            <td>{{$negocio->name}}</td>
                            <td>{{$negocio->item->description}}</td>
                            <td>{{$negocio->owner}}</td>
                            <td>{{$negocio->phone}}</td>
                            <td>{{$negocio->location->description}}</td>

                            <td>
                                <button data-toggle="modal" data-target="#updateModal" wire:click="edit({{$negocio->id}})" style="margin: 5px" class="btn btn-sm btn-outline-primary py-2"> <i class="far fa-eye"></i> </button>
                            </td>

                             <td>
                                @if ($estadoBan != 1)
                                    <button wire:click="accept({{$negocio->id}})" style="margin: 5px" class="btn btn-sm btn-outline-primary py-2"> <i class="fas fa-thumbs-up"></i> </button>
                                @elseif ($estadoBan != 2)
                                    <button wire:click="deny({{$negocio->id}})" style="margin: 5px" class="btn btn-sm btn-outline-danger py-2"> <i class="fas fa-thumbs-down "style="color:red"></i> </button>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>
                <div class="d-flex justify-content-center text-center">
                    Desde {{$negocios->firstItem()}} a {{$negocios->lastItem()}} de {{ $negocios->total() }} registros
                </div>
                <div class="d-flex float-right">
                    {{ $negocios->links() }}

                </div>

                @elseif($search)
            <br><br>
            <div class="alert alert-danger d-flex justify-content-center text-center" role="alert">
                <h3>No se encontraron registros para "{{$search}}"</h3>
            </div>
                    @else
            <br><br>
            <div class="alert alert-danger d-flex justify-content-center text-center" role="alert">
                <h3>No hay registros cargados</h3>
            </div>
        @endif
                
        </div>
        
         

</div>
</div>
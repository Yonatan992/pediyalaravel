<!-- Modal -->
<div wire:ignore.self class="modal fade" id="updateModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-info">
				<h5 class="modal-title" id="exampleModalLabel">Modificar categoria</h5>
				<button type="button" wire:click.prevent="cancel()" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					<form>
						<div class="container">
							<div class="card">
								<input type="hidden" wire:model="selected_id">

								<div class="col-md-12">
									<div class="form-group">
										<label >Categoria</label>
										<select wire:model="categoria" class="custom-select">
											@foreach($categorias as $categoria)
											<option value="{{ $categoria->id }}">{{ $categoria->description }}</option>
											@endforeach
										</select>
										@error('categoria') <span class="text-danger error">* {{ $message }}</span>@enderror
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label >Descripcion</label>
										<input type="text" wire:model="description" class="form-control input-sm"  placeholder="Descripcion">
										@error('description') <span class="text-danger error">* {{ $message }}</span>@enderror
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" wire:click.prevent="update()" class="btn btn-primary close-modal"><i class="fas fa-save"> Modificar</i></button>
				<button type="button" wire:click.prevent="cancel()" class="btn btn-danger close-btn" data-dismiss="modal"><i class="fas fa-times-circle"> Cerrar</i></button>

			</div>
		</div>
	</div>
</div>
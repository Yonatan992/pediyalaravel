<div class="container">
    @include('livewire.states.create')
    @include('livewire.states.update')
    <div class="card p-2">
        @if(session()->has('message'))
        <div class="alert alert-success">{{ session('message') }}</div>
        @endif

    <div class="table-responsive-sm">
            <form class="form-inline d-flex float-right md-form form-sm mt-0">
                <i class="fas fa-search" aria-hidden="true"></i>
                <input class="form-control form-control-sm ml-3 w-75" style="margin: 10px" type="text" placeholder="Buscar" wire:model="search" aria-label="Search">
            </form>
    @if ($estados->count())
        <br>
            <div class="col form-inline p-2">
                Mostrar &nbsp;
                        <select wire:model="perPage" class="form-control">
                            <option>5</option>
                            <option>10</option>
                            <option>15</option>
                        </select>
                        &nbsp; registros      
            </div>
        <table class="table table-striped table-bordered table-hover">
                    <thead class="thead-dark">
                <tr>
                    <th>Descripción</th>
                    <th>Acciones</th>
                </tr>
            </thead>
           
            <tbody align="center">
                @foreach($estados as $estado)
                    <tr>
                        <td>{{$estado->description}}</td>
                        <td>
                            <button data-toggle="modal" style="margin: 5px" data-target="#updateModal" wire:click="edit({{$estado->id}})" class="btn btn-sm btn-outline-primary py-2"> <i class="fa fa-edit blue"> </i>  </button> 
                            <button wire:click="destroy({{$estado->id}})" style="margin: 5px" class="btn btn-sm btn-outline-danger py-2"> <i class="fa fa-trash red"></i> </button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="d-flex justify-content-center text-center">
                    Desde {{$estados->firstItem()}} a {{$estados->lastItem()}} de {{ $estados->total() }} registros
                </div>

            <div class="d-flex float-right">
                    {{ $estados->links() }}

            </div>
    </div>
    
    @else
        <br><br>
            <div class="alert alert-danger" role="alert">
                <h5>No se encontraron registros para "{{$search}}"</h5>
            </div>
    @endif

</div>

</div>
<div class="container">
    <div class="row">
        <button type="button" class="btn btn-primary btn-md-2" style="margin: 8px" data-toggle="modal" data-target="#stateModal">
            <i class="fas fa-plus-square"></i> Nuevo Estado
        </button>
    </div>
</div>

<!-- Modal -->
<div wire:ignore.self class="modal fade" id="stateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <h5 class="modal-title" id="exampleModalLabel">Nuevo estado</h5>
                <button type="button" wire:click.prevent="cancel()" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
           <div class="modal-body">
                <form>
                    @csrf
                    <div class="form-group">
                        <label >Descripcion</label>
        				<input type="text" wire:model="description" class="form-control input-sm"  placeholder="Descripcion">
                        @error('description') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" wire:click.prevent="store()" class="btn btn-primary close-modal"><i class="fas fa-save"> Guardar</i> </button>
                <button type="button" wire:click.prevent="cancel()" class="btn btn-danger close-btn" data-dismiss="modal"><i class="fas fa-times-circle"> Cerrar</i></button>
                
            </div>
        </div>
    </div>
</div>
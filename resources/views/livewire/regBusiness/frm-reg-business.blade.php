
<section class="contenedor-registro">
	<div class="card">
		 @if(session()->has('message'))
		 	<br>
		   	<div class="alert alert-success">{{ session('message') }}</div>
		  	@endif
	</div>
	<div wire:ignore.self class="box">
		<h2>Registro Negocio</h2>
		<small>Por favor, brindanos los siguientes datos sobre el comercio y nos pondremos en contacto lo antes posible.</small>
		<form>
			@csrf
			<div class="inputBox">
				<input type="text" wire:model="propietario" class="text-capitalize" name="duenio" autofocus required>
				<label for="duenio" class="label-name">
					<span class="content-name">Propietario</span>
				</label>

			</div>
			<h4>@error('propietario') <span class="text-danger">* {{ $message }}</span>@enderror</h4>
			<div class="inputBox">
				<input type="number" wire:model="tel" name="tel" required>
				<label for="tel" class="label-name">
					<span class="content-name">Teléfono</span>
				</label>
			</div>
			<h4>@error('tel') <span class="text-danger">* {{ $message }}</span>@enderror</h4>

			<div class="inputBox">
				<input type="text" wire:model="nombreCo"class="text-capitalize" name="nomComercio" required>
				<label for="nomComercio" class="label-name">
					<span class="content-name">Nombre del comercio</span>
				</label>
			</div>
			<h4>@error('nombreCo') <span class="text-danger">* {{ $message }}</span>@enderror</h4>

			<div class="inputBox">

				<select name="rubro" wire:model="rubro" id="rubro">
					<option>Seleccione un rubro...</option>
					@foreach ($items as $item)
					<option value="{{$item->id}}">{{$item->description}}</option>
					@endforeach
				</select>
				<label for="rubro" class="label-name">
					<span class="content-name">Tipo de comercio</span>
				</label>
			</div>
			<h4>@error('rubro') <span class="text-danger">* {{ $message }}</span>@enderror</h4>

			<div class="inputBox">
				<select wire:model="provinceId" class="custom-select">
					<option value="">Seleccione una provincia...</option>
					@foreach ($provinces as $province)
					<option value="{{ $province->id }}">{{$province->description}}</option>
					@endforeach
				</select>
				<label for="provincia" class="label-name">
					<span class="content-name">Provincia</span>
				</label>
			</div>
			<h4>@error('locationId') <span class="text-danger">* {{ $message }}</span>@enderror</h4>

			@if(count($this->locations) > 0) 
				<div class="inputBox">
					
						<select wire:model="locationId" class="custom-select">
							@foreach ($locations as $location)
							<option value="{{$location->id}}">{{$location->description}}</option>
							@endforeach
						</select>
					
					<label for="localidad" class="label-name">
						<span class="content-name">Localidad</span>
					</label>
				</div>
				<h4>@error('locationId') <span class="text-danger">* {{ $message }}</span>@enderror</h4>
			@endif
			<div class="inputBox">
				<input class="text-capitalize" wire:model="direccion" type="text" name="dirComercio" required>
				<label for="dirComercio" class="label-name">
					<span class="content-name">Dirección del comercio</span>
				</label>
			</div>
			<h4>@error('direccion') <span class="text-danger">* {{ $message }}</span>@enderror</h4>

			<div class="inputBox">
				<input type="text" wire:model="email" name="email" required>
				<label for="email" class="label-name">
					<span class="content-name">Email</span>
				</label>
			</div>
			<h4>@error('email') <span class="text-danger">* {{ $message }}</span>@enderror</h4>
			<br>
			<button type="button" wire:click.prevent="store()" class="inputButton"><i class="fas fa-save"> <span id="nom">Registrar </span></i> </button>
			<br>
			<button type="button" wire:click.prevent="cancel()" class="inputButton"><i class="fas fa-times-circle"> Cancelar</i></button>
		</form>
	</div>
</section>

<!-- Modal -->
<div wire:ignore.self class="modal fade" id="updateModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
       <div class="modal-content">
            <div class="modal-header bg-info">
                <h5 class="modal-title" id="exampleModalLabel">Modificar Rubro</h5>
                <button type="button" wire:click.prevent="cancel()" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            	<form>
					<input type="hidden" wire:model="selected_id">
					<div class="form-group">
						<label for="description">Description</label>
						<input type="text" wire:model="description" class="form-control input-sm" placeholder="Descripcion">
						@error('description') <span class="text-danger">{{ $message }}</span>@enderror
					</div>
					
				</form>
                
            </div>
            <div class="modal-footer">
                 <button type="button" wire:click.prevent="update()" class="btn btn-primary close-modal"><i class="fas fa-save"> Modificar</i></button>
            	 <button type="button" wire:click.prevent="cancel()" class="btn btn-danger close-btn" data-dismiss="modal"><i class="fas fa-times-circle"> Cerrar</i></button>
        
            </div>
       </div>
    </div>
</div>
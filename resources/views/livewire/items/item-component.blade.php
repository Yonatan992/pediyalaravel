<div class="container">
    @include('livewire.items.create')
    @include('livewire.items.update')
    <div class="card p-2">
        @if(session()->has('message'))
        <div class="alert alert-success">{{ session('message') }}</div>
        @endif
        <div class="table-responsive-sm">
            <form class="form-inline d-flex float-right md-form form-sm mt-0">
                <i class="fas fa-search" aria-hidden="true"></i>
                <input class="form-control form-control-sm ml-3 w-75" style="margin: 10px" type="text" placeholder="Buscar" wire:model="search" aria-label="Search">
            </form>
            @if ($rubros->count())
            <br>
                    <div class="col form-inline p-2">
                        Mostrar &nbsp;
                        <select wire:model="perPage" class="form-control">
                            <option>5</option>
                            <option>10</option>
                            <option>15</option>
                        </select>
                        &nbsp; registros      
                    </div>
            <table class="table table-striped table-bordered table-hover">
                    <thead class="thead-dark">
                    <tr>
                        <th><a class="text-light" wire:click.prevent="sortBy('description')" role="button" href="#">
                            Descripción&nbsp;
                            @include('livewire.categories._sort-icon',['field'=>'description'])
                            </a>
                        </th>
                        <th>Acciones</th>
                    </tr>
                </thead>

                <tbody align="center">
                    @foreach($rubros as $rubro)
                    <tr>
                        <td>{{$rubro->description}}</td>
                        <td>
                            <button data-toggle="modal" style="margin: 5px" data-target="#updateModal" wire:click="edit({{$rubro->id}})" class="btn btn-sm btn-outline-primary py-2"> <i class="fa fa-edit blue"> </i>  </button> 
                            <button wire:click="destroy({{$rubro->id}})" style="margin: 5px" class="btn btn-sm btn-outline-danger py-2"> <i class="fa fa-trash red"></i> </button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="d-flex justify-content-center text-center">
                    Desde {{$rubros->firstItem()}} a {{$rubros->lastItem()}} de {{ $rubros->total() }} registros
                </div>

            <div class="d-flex float-right">
                    {{ $rubros->links() }}

            </div>
        </div>

        @else
        <br><br>
            <div class="alert alert-danger" role="alert">
                <h5>No se encontraron registros para "{{$search}}"</h5>
            </div>
        @endif

    </div>

</div>
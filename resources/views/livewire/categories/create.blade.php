<div class="container">
	<div class="row">
			<button type="button" class="btn btn-primary btn-md-2" style="margin: 8px" data-toggle="modal" data-target="#categoryModal">
			<i class="fas fa-plus-square"></i> Nueva Categoria
			</button>
	
		
			<button type="button" class="btn btn-secondary btn-md-2" style="margin: 8px" data-toggle="modal" data-target="#subCategoryModal">
				<i class="fas fa-plus-square"></i> Nueva Subcategoría
			</button>
	

	</div>
</div>
<div class="container">
	<!-- Modal -->
<div wire:ignore.self class="modal fade" id="categoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header bg-info">
				<h5 class="modal-title" id="exampleModalLabel">Nueva categoria</h5>
				<button type="button" wire:click.prevent="cancel()" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true close-btn">×</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					
						<form>
							@csrf
							<div class="container">
							<div class="card">
								
								<div class="form-group row">
									<div class="form-group col-md-4">
									<label >Rubro</label>
									<br>
									<select wire:model="rubro" class="custom-select">
										<option value="" selected>Seleccione rubro</option>
										@foreach($rubros as $rubro)
										<option value="{{ $rubro->id }}">{{ $rubro->description }}</option>
										@endforeach
									</select>
									@error('rubro') <span class="text-danger error">* {{ $message }}</span>@enderror
								</div>

								<div class="form-group col-md-8">
									<label >Descripcion</label>
									<br>
									<input type="text" wire:model="description" class="form-control input-sm"  placeholder="Descripcion">
									@error('description') <span class="text-danger error">* {{ $message }}</span>@enderror
								</div>
									

								</div>

								<div class="form-group col-md-12">
									@if ($avatar)
										<img src="{{ $avatar->temporaryUrl() }}" class="rounded mx-auto d-block" width="250" height="150" >
									@else 
										<img src="{{ asset('img/logos/upload.png') }}" class="rounded mx-auto d-block" width="150" height="150" >
									@endif
								</div>
								<div class="form-group col-md-6">
									<div class="custom-file">
										<input type="file" wire:model="avatar" class="custom-file-input" id="customFileLang">
										<label class="custom-file-label" for="customFileLang">Seleccionar Imagen</label>
									</div>
									@error('avatar') <span class="text-danger error">* {{ $message }}</span>@enderror
								</div>

								
								
							</div>
							</div>
						</form>
					
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" wire:click.prevent="store()" class="btn btn-primary close-modal"><i class="fas fa-save"> Guardar</i> </button>
				<button type="button" wire:click.prevent="cancel()" class="btn btn-danger close-btn" data-dismiss="modal"><i class="fas fa-times-circle"> Cerrar</i></button>

			</div>
		</div>
	</div>
</div>
	
</div>

<!-- Modal -->
<div wire:ignore.self class="modal fade" id="subCategoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header bg-info">
				<h5 class="modal-title" id="exampleModalLabel">Nueva Subcategoría</h5>
				<button type="button" wire:click.prevent="cancel()" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true close-btn">×</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					
					<form>
						@csrf
						<div class="container">
							<div class="card">
								<div class="form-group row">
									<div class="form-group col-md-6">
									<label >Rubro</label>
									<br>
									<select wire:model="itemId" class="custom-select">
										<option value="" selected>Seleccione rubro</option>
										@foreach($itemsArray as $rubro)
										<option value="{{ $rubro->id }}">{{ $rubro->description }}</option>
										@endforeach
									</select>
									@error('rubro') <span class="text-danger error">* {{ $message }}</span>@enderror
								</div>

								 
									<div class="form-group col-md-6">
										<label >Categoria</label>
										<br>
										@if(count($this->categoriesArray) > 0)
										<select wire:model="category" class="custom-select">
											<option value="" selected>Seleccione Categoría</option>
											@foreach($categoriesArray as $categoria)
											<option value="{{ $categoria->id }}">{{ $categoria->description }}</option>
											@endforeach
										</select>
										@endif
										@error('category') <span class="text-danger error">* {{ $message }}</span>@enderror
									</div>
								
									
								</div>
								

								<div class="form-group col-md-6">
									<label >Descripcion</label>
									<br>
									<input type="text" wire:model="descriptionCat" class="form-control input-sm"  placeholder="Descripcion">
									@error('descriptionCat') <span class="text-danger error">* {{ $message }}</span>@enderror
								</div>
								
							</div>
						</div>
					</form>
					
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" wire:click.prevent="storeSubCategory()" class="btn btn-primary close-modal"><i class="fas fa-save"> Guardar</i> </button>
				<button type="button" wire:click.prevent="cancel()" class="btn btn-danger close-btn" data-dismiss="modal"><i class="fas fa-times-circle"> Cerrar</i></button>

			</div>
		</div>
	</div>
</div>
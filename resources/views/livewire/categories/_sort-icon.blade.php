@if($sortField !== $field)
	<i class="text-white fas fa-sort fa-lg" ></i>
@elseif ($sortAsc)
	<i class="text-white fas fa-sort-up fa-lg" ></i>
@else
	<i class="text-white fas fa-sort-down fa-lg" ></i>
@endif
<!-- Modal -->
<div wire:ignore.self class="modal fade" id="updateModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-info">
				<h5 class="modal-title" id="exampleModalLabel">Modificar categoria</h5>
				<button type="button" wire:click.prevent="cancel()" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					<form>
						<div class="container">
						<div class="card">
							<input type="hidden" wire:model="selected_id">
							
							<div class=" form-group row">
								<div class="form-group col-md-6">
								<label >Rubro</label>
								<br>
								<select wire:model="rubro" class="custom-select">
									@foreach($rubros as $rubro)
									<option value="{{ $rubro->id }}">{{ $rubro->description }}</option>
									@endforeach
								</select>
								@error('rubro') <span class="text-danger error">* {{ $message }}</span>@enderror
							</div>
							
							<div class="form-group col-md-6">
								<label >Descripcion</label>
								<br>
								<input type="text" wire:model="description" class="form-control input-sm"  placeholder="Descripcion">
								@error('description') <span class="text-danger error">* {{ $message }}</span>@enderror
							</div>
								
							</div>
								<div class="form-group row">
								<div class="form-group col-md-6 p-3">

								<label>Imagen actual</label>
								<br><br>
								<img src="{{ $imagenActual}}" class="rounded mx-auto d-block" width="150" height="150" >
								</div>

								<div class="form-group col-md-6 p-3">
										<label>Imagen nueva</label>
										<br>
										@if ($avatar)
										<br>
											<img src="{{ $avatar->temporaryUrl() }}" class="rounded mx-auto d-block" width="150" height="150" >
										@else 
											<img src="{{ asset('img/logos/upload.png') }}" class="rounded mx-auto d-block" width="150" height="150" >
										@endif
										<br>
										<div class="custom-file">
											<input type="file" wire:model="avatar" class="custom-file-input" id="customFileLang">
											<br>
											<label class="custom-file-label" for="customFileLang">Reemplazar Imagen</label>
										</div>
										@error('avatar') <span class="text-danger error">* {{ $message }}</span>@enderror
								</div>
							</div>
								
							
							
							
							
						</div>
					</div>
					</form>
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" wire:click.prevent="update()" class="btn btn-primary close-modal"><i class="fas fa-save"> Modificar</i></button>
				<button type="button" wire:click.prevent="cancel()" class="btn btn-danger close-btn" data-dismiss="modal"><i class="fas fa-times-circle"> Cerrar</i></button>

			</div>
		</div>
	</div>
</div>
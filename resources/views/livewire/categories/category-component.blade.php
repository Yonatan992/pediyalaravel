<div class="container">
    @include('livewire.categories.create')
    @include('livewire.categories.update')
    @include('livewire.categories.create-subcategory')
    <div class="card p-2">
        @if(session()->has('message'))
        <br>
        <div class="alert alert-success">{{ session('message') }}</div>
        @endif
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="#">Categoria</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Subcategorías</a>
            </li>
        </ul>
        
        <div class="table-responsive-sm">
            <form class="form-inline d-flex float-right md-form form-sm mt-0">
                <i class="fas fa-search" aria-hidden="true"></i>
                <input class="form-control form-control-sm ml-3 w-75" style="margin: 10px" type="text" placeholder="Buscar" wire:model="search" aria-label="Search">
            </form>
            @if ($categorias->count())
            <br>
                    <div class="col form-inline p-2">
                        Mostrar &nbsp;
                        <select wire:model="perPage" class="form-control">
                            <option>5</option>
                            <option>10</option>
                            <option>15</option>
                        </select>
                        &nbsp; registros       
                    </div>
                    
                <table class="table table-striped table-bordered table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th><a class="text-light" wire:click.prevent="sortBy('description')" role="button" href="#">
                            Descripción&nbsp;
                            @include('livewire.categories._sort-icon',['field'=>'description'])
                            </a></th>
                            <th>Rubro</th>
                            <th>Imagen</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody align="center">
                        @foreach($categorias as $categoria)
                        <tr>
                            <td>{{$categoria->description}}</td>
                            <td>{{$categoria->item->description}}</td>
                            <td > <img  src="{{$categoria->image}}" class="card" width="50" height="50" alt="Imagen no Disponible" /></td>

                            <td>
                                <button data-toggle="modal" data-target="#updateModal" wire:click="edit({{$categoria->id}})" style="margin: 5px" class="btn btn-sm btn-outline-primary py-2"> <i class="fa fa-edit blue"> </i> </button>

                                <button wire:click="destroy({{$categoria->id}})" style="margin: 5px" class="btn btn-sm btn-outline-danger py-2"> <i class="fa fa-trash red"></i> </button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>
                <div class="d-flex justify-content-center text-center">
                    Desde {{$categorias->firstItem()}} a {{$categorias->lastItem()}} de {{ $categorias->total() }} registros
                </div>
                <div class="d-flex float-right">
                    {{ $categorias->links() }}

                </div>
                
        </div>
        
        @else
        <br><br>
            <div class="alert alert-danger" role="alert">
                <h5>No se encontraron registros para "{{$search}}"</h5>
            </div>
        @endif
</div>
</div>
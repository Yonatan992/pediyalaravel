<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Bienvenido a Entra y Pedí </title>
</head>
<body>
    <p>Hola! <b>{{$business->owner}}</b>:</p>
    <p>Su comercio <b>{{$business->name}}</b> se encuentra habilitado para operar.
    </p>
    <p>
        Atte. el equipo Entra y Pedí.
    </p>
    <p>
        Muchas Gracias.
    </p>
</body>
</html>
<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Bienvenido a Entra y Pedí </title>
</head>
<body>
    <p>Hola! <b>{{$business->owner}}</b>:</p><br>
    <p>La solicitud de su comercio <b>{{$business->name}}</b> está haciendo analizada.Dentro de las 24 hs. Nos comunicaremos con usted. No conteste este email.
    </p><br>
    <p>
        Atte. el equipo Entra y Pedí.
    </p> <br>
    <p>
        Muchas Gracias.
    </p>
</body>
</html>